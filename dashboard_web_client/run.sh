#!/usr/bin/env bash

#Disable npm progress bar - for speed.
npm set progress=false

npm install

# Start the npm server
make run
