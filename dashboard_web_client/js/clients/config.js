export default function (nga, admin) {
    var order_status_choices = [
        {
            "label": "Cancelada",
            "value": 0
        },
        {
            "label": "Pendiente",
            "value": 1
        },
        {
            "label": "Completada",
            "value": 2
        }
    ];
    var order_payment_status_choices = [

        {
            "label": "Pagado",
            "value": true
        },
        {
            "label": "Pendiente",
            "value": false
        }
    ];
    var clients = admin.getEntity('clients');
    clients.listView()
        .title('Clientes')
        .fields([
            nga.field('username').isDetailLink(true)
                .label('Nombre de usuario'),
            nga.field('first_name')
                .label('Nombre'),
            nga.field('last_name')
                .label('Apellido'),
            nga.field('email')
                .label('Email'),
            nga.field('address.street')
                .label('Direccion'),
            nga.field('orders', 'referenced_list') // display list of related comments
                .perPage(200)
                .label('Ordenes')
                .targetEntity(admin.getEntity('orders'))
                .targetReferenceField('client')
                .targetFields([
                    nga.field('id')
                        .isDetailLink(true)
                        .label('id'),
                    nga.field('created_at', 'date')
                        .format('dd-MM-yyyy')
                        .isDetailLink(true)
                        .label('Fecha de creacion'),
                    nga.field('status', 'choice').choices(order_status_choices)
                        .label('Estado'),
                    nga.field('payed').map(function truncate(value, entry) {
                        if(value){ return 'Pagado'} else { return 'Pendiente'}
                    })
                        .label('Estado de Pago'),
                    nga.field('total', 'amount')
                        .label('Total'),
//                        nga.field('stock', 'template')
//                            .label('Accion stock')
//                            .template(`<ma-create-button entity-name="IOProductsStock" default-values="{ stock: entry.values.stock }" size="xs" label="Agregar stock"></ma-create-button>
//                                <ma-show-button entity-name="productsStock" entry="{ identifierValue: entry.values.stock}" size="xs" label="Detalle stock"></ma-create-button>`)

                ])
                .sortField('created_at')
                .sortDir('DESC'),

        ])
        .filters([
            nga.field('search', 'template')
                .label('')
                .pinned(true)
                .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Buscar" class="form-control"></input><span class="input-group-addon"><i class="fa fa-search"></i></span></div>'),
        ])
        .listActions(['edit', 'delete']);

    clients.showView()
        .title('Detalle del usuario {{ entry.values.username }}')
        .fields(clients.listView().fields());


    // clients.creationView()
    //     .title('Crear Cliente')
    //     .fields([
    //         nga.field('first_name')
    //             .label('Nombre'),
    //         nga.field('address.street')
    //             .label('Direccion')
    //         ]);

    // clients.editionView()
    //     .title('Editando a {{ entry.values.first_name }}')
    //     .fields([
    //         nga.field('first_name')
    //             .label('Nombre'),
    //         nga.field('address.street')
    //             .label('Direccion'),
    //         // nga.field('balance', 'reference')
    //         //   .isDetailLink(true).editable(false)
    //         //   .label('Balance')
    //         //   .targetEntity(admin.getEntity('balances'))
    //         //   .targetField(nga.field('id', 'template').template('<p>Ver balance</p>'))
    //         //   .singleApiCall(ids => ({'id': ids }))
    //     ]);

    return clients;
}
