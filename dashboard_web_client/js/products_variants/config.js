
export default function (nga, admin) {

    var productsVariants = admin.getEntity('productsVariants')
        .label('Variaciones de Productos');
    productsVariants.listView()
        .title('Variaciones de Productos')
        .fields([
            nga.field('sku', 'text'),
            nga.field('name', 'text')
                .label('Nombre')
                .map(function truncate(value, entry) {
                    return entry.display_name;
                })
                .isDetailLink(true),
            nga.field('attributes_display')
                .map(function truncate(value, entry) {
                    return entry.attributes_display;
                })
                .label('Attributos'),
            nga.field('price', 'amount')
                .label('Precio'),
            nga.field('stock_quantity', 'number')
                .label('Stock')
        ]).filters([
        nga.field('search', 'template')
            .label('')
            .pinned(true)
            .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Buscar" class="form-control"></input><span class="input-group-addon"><i class="fa fa-search"></i></span></div>'),
        nga.field('price_lte', 'float')
            .label('Precio menor a'),
        nga.field('price_gte', 'float')
            .label('Precio mayor a'),
        nga.field('stock_lte', 'number')
            .label('Stock menor a')
            .defaultValue(10),
        nga.field('category', 'reference')
            .perPage(200)
            .label('Categoria')
            .targetEntity(admin.getEntity('categories'))
            .targetField(nga.field('name'))
    ]).listActions([
        '<ma-create-button entity-name="IOProductsStock" default-values="{ stock: entry.values.stock }" size="xs" label="Agregar stock"></ma-create-button>',
        '<ma-show-button entity-name="productsStock" entry="{ identifierValue: entry.values.stock}" size="xs" label="Detalle stock"></ma-create-button>',
        //'<ma-edit-button entry="entry" entity="entity" label="Edit me" size="xs"></ma-edit-button>',
        //'<ma-delete-button entry="entry" entity="entity" label="Delete me" size="xs"></ma-delete-button>',
    ]);
    productsVariants.creationView()
        .title('Crear variacion de producto')
        .fields([
            nga.field('name')
                .label('Nombre')
                .validation({required: true }),
            nga.field('sku')
                .label('SKU')
                .validation({required: true }),
            nga.field('product', 'reference')
                .perPage(200)
                .targetEntity(admin.getEntity('products'))
                .targetField(nga.field('name')),
            nga.field('price', 'float')
                .label('Precio')
                .map(function truncate(value, entry) {
                    //return parseFloat(value).toFixed(2);
                    return parseFloat(value);
                })
                .validation({required: true}),
            nga.field('attributes', 'template')
                .template('<attribute-field field="::field" value="::entry.values[field.name()]"></attribute-field>')
        ]).onSubmitSuccess(['progression', 'notification', '$state', 'entry', 'entity', function(progression, notification, $state, entry, entity) {
            // stop the progress bar
            progression.done();
            // add a notification
            notification.log(`Se agrego #${entry._identifierValue} exitosamente.`, { addnCls: 'humane-flatty-success' });
            // redirect to the list view
            //$state.go($state.get('list'), { entity: entity.name() });
            $state.go($state.get('edit'), { entity: 'products', 'id': entry.values.product });
            // cancel the default action (redirect to the edition view)
            return false;
    }]);

    productsVariants.editionView()
        .title('Editar {{ entry.values.display_name }}')
        .fields(
            nga.field('name')
                .label('Nombre')
                .validation({required: true }),
            nga.field('sku')
                .label('SKU')
                .validation({required: true }),
            nga.field('product', 'reference')
                .perPage(200)
                .editable(false)
                .targetEntity(admin.getEntity('products'))
                .targetField(nga.field('name')),
            nga.field('price')
                .label('Precio')
                .validation({required: true}),
            nga.field('attributes', 'template')
                .template('<attribute-field field="::field" value="::entry.values[field.name()]"></attribute-field>')
            //nga.field('attributes', 'attributes')
    );

    return productsVariants;
}
