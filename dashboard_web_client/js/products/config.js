
export default function (nga, admin) {

    var products = admin.getEntity('products')
        .label('Productos');
    products.listView()
        .title('Productos')
        .fields([
            nga.field('images', 'template')
                .isDetailLink(true)
                .template(`<img ng-if="entry.values.images.length" ng-src="{{ entry.values.images[0].image }}" width="25" style="margin-top:-5px" />
                    <img ng-if="!entry.values.images.length" src="/images/product_default.jpg" width="25" style="margin-top:-5px" />`),
            nga.field('name', 'text')
                .isDetailLink(true)
                .label('Nombre'),
            nga.field('category', 'reference')
          .perPage(200)
            .label('Categoria')
            .targetEntity(admin.getEntity('categories'))
            .targetField(nga.field('name')),
            nga.field('price', 'amount')
                .format('0.00')
                .currency('$')
                .label('Precio'),
        ]).filters([
        nga.field('search', 'template')
            .label('')
            .pinned(true)
            .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Buscar" class="form-control"></input><span class="input-group-addon"><i class="fa fa-search"></i></span></div>'),
        nga.field('category', 'reference')
            .perPage(200)
            .label('Categoria')
            .targetEntity(admin.getEntity('categories'))
            .targetField(nga.field('name'))
    ]);

    products.creationView()
        .title('Crear producto')
        .fields([
            nga.field('name')
                .label('Nombre')
                .validation({required: true }),
            nga.field('outstanding', 'boolean')
                .defaultValue(false)
                .choices([
                    { value: true, label: 'Si' },
                    { value: false, label: 'No' }
                ])
                .label("Destacar"),
            nga.field('category', 'reference')
                .perPage(200)
                .targetEntity(admin.getEntity('categories'))
                .targetField(nga.field('name')),
            nga.field('price', 'float')
                .format('0.00')
                .label('Precio')
                .validation({required: true}),
            nga.field('attributes', 'reference_many')
                .perPage(200)
                .targetEntity(admin.getEntity('productsAttributes'))
                .targetField(nga.field('name')),
            nga.field('generate_variation', 'boolean')
                .defaultValue(false)
                .choices([
                    { value: true, label: 'Si' },
                    { value: false, label: 'No' }
                ])
                .label("Generar todas las variaciones disponibles"),
            nga.field('description', 'wysiwyg')
                .label('Descripcion')
        ]);

    products.editionView()
        .title('Editar {{ entry.values.name }} # {{ entry.values.id }}')
        .fields([
            nga.field('name')
                .label('Nombre')
                .validation({required: true }),
            nga.field('outstanding', 'boolean')
                .choices([
                    { value: true, label: 'Si' },
                    { value: false, label: 'No' }
                ])
                .label("Destacar"),
            nga.field('category', 'reference')
                .perPage(200)
                .targetEntity(admin.getEntity('categories'))
                .targetField(nga.field('name')),
            nga.field('price', 'amount')
                .format('0.00')
                .currency('$')
                .label('Precio'),
            nga.field('description', 'wysiwyg')
                .label('Descripcion'),


            nga.field('display_image', 'template')
                .label('Images')
                .template(`<upload-images  uploaded-images="{{ entry.values.images }}" files-model="images" item-id="{{ entry.values.id }}"></upload-images>`)
                .editable(false),

            nga.field('variations', 'referenced_list') // display list of related comments
                    .perPage(200)
                    .label('Variaciones')
                    .targetEntity(admin.getEntity('productsVariants'))
                    .targetReferenceField('product')
                    .targetFields([
                        nga.field('sku')
                            .isDetailLink(true)
                            .label('SKU'),
                        nga.field('display_name')
                            .label('Nombre'),
                        nga.field('attributes_display')
                            .label('Attributos'),
                        nga.field('stock_quantity', 'number')
                            .label('Stock'),
                        nga.field('stock', 'template')
                            .label('Accion stock')
                            .template(`<ma-create-button entity-name="IOProductsStock" default-values="{ stock: entry.values.stock }" size="xs" label="Agregar stock"></ma-create-button>
                                <ma-show-button entity-name="productsStock" entry="{ identifierValue: entry.values.stock}" size="xs" label="Detalle stock"></ma-create-button>`)
                 
                    ])
                    .sortField('created_at')
                    .sortDir('DESC'),
            nga.field('Acciones', 'template')
              .template(`<ma-create-button entity-name="productsVariants" default-values="{ product: entry.values.id }" size="xs" label="Agregar variacion"></ma-create-button>
                    <ma-filtered-list-button entity-name="productsVariants" filter="{ product: entry.values.id }" size="xs" label="Ver listado de variaciones"></ma-filtered-list-button>`),

            
            
    ]);

    return products;
}
