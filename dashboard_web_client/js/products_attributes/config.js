
export default function (nga, admin) {

    var widget_types_choices = [
        {
            "label": "Text",
            "value": "TXT"
        },
        // {
        //     "label": "Image",
        //     "value": "IMG"
        // },
        {
            "label": "Color",
            "value": "COL"
        }
    ];

    var productsAttributes = admin.getEntity('productsAttributes')
        .label('Productos');
    productsAttributes.listView()
        .title('Productos')
        .fields([
            nga.field('name', 'text')
                .isDetailLink(true)
                .label('Nombre'),
            nga.field('display', 'text')
                .label('Display'),
            nga.field('widget_type', 'choice').choices(widget_types_choices)
                .label('Tipo')
        ]).filters([
        nga.field('search', 'template')
            .label('')
            .pinned(true)
            .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Buscar" class="form-control"></input><span class="input-group-addon"><i class="fa fa-search"></i></span></div>'),
    ]);

    productsAttributes.creationView()
        .title('Crear Atributo de producto')
        .fields([
            nga.field('name')
                .label('Nombre'),
            nga.field('display')
                .label('Display'),
            nga.field('widget_type', 'choice').choices(widget_types_choices)
                .label('Tipo'),
            nga.field('values', 'embedded_list')
              .targetFields([ 
                  nga.field('display')
                    .label('Display'),
                  nga.field('color')
              ])
        ]);

    productsAttributes.editionView()
        .title('Editar {{ entry.values.name }}')
        .fields(
            productsAttributes.creationView().fields()
    ).onSubmitError(['error', 'form', 'progression', 'notification', function(error, form, progression, notification) {
        // mark fields based on errors from the response
        console.log("ERRORS");
        console.log(error);
        console.log(error.data);
        angular.forEach(error.data, function(value, key) {
          console.log("VIOLATION");
            console.log(key);
            if (form[key]) {
                form[key].$valid = false;
                form[key].message_error = String(value);
                console.log(value);
            }
        });
        // stop the progress bar
        progression.done();
        // add a notification
        notification.log(`Some values are invalid, see details in the form`, { addnCls: 'humane-flatty-error' });
        // cancel the default action (default error messages)
        return false;
    }]);

    return productsAttributes;
}
