export default function (nga, admin) {

    var payment_types_choices = [
        {
            "label": "Efectivo",
            "value": "cash"
        },
        {
            "label": "Cheque",
            "value": "check"
        },
        {
            "label": "Tarjeta de Credito",
            "value": "credit_card"
        },
        {
            "label": "Tranferencia",
            "value": "transfer"
        }
    ];

    var payment_status_choices = [
        {
            "label": "Cancelado",
            "value": 0
        },
        {
            "label": "Pendiente",
            "value": 1
        },
        {
            "label": "Pagado",
            "value": 2
        }
    ];

    var payments = admin.getEntity('payments');
    payments.listView()
        .title('Pagos')
        .fields([
            nga.field('date', 'date')
                .isDetailLink(true)
                .format('dd-MM-yyyy')
                .label('Fecha'),
            nga.field('amount', 'amount')
                .label('Monto'),
            nga.field('type', 'choice').choices(payment_types_choices)
                .label('Tipo'),
            nga.field('order', 'reference')
                .label('Orden')
                .targetEntity(admin.getEntity('orders'))
                .targetField(nga.field('id'))
                .singleApiCall(ids => ({'id': ids }))
        ])
        .listActions(['edit', 'delete']);

    payments.creationView()
        .title('Crear pago')
        .fields([
            nga.field('amount', 'float')
                .label('Monto')
                .validation({required: true}),
            nga.field('status', 'choice').choices(payment_status_choices).defaultValue(2),
//            nga.field('status', 'boolean').validation({required: true}).defaultValue(true),
//            nga.field('status', 'boolean'),
            nga.field('date', 'datetime')
                .defaultValue(new Date(Date.now()))
                .label('Fecha')
                .validation({required: true}),
            nga.field('type', 'choice').choices(payment_types_choices)
                .defaultValue('cash')
                .label('Tipo')
                .validation({required: true}),
            nga.field('order', 'reference')
                .perPage(200)
                .permanentFilters({ status: 1 })
                .targetEntity(admin.getEntity('orders'))
                .targetField(nga.field('id'))
                .validation({required: true})
                .singleApiCall(ids => ({'id': ids }))

        ]).onSubmitSuccess(['progression', 'notification', '$state', 'entry', 'entity', function(progression, notification, $state, entry, entity) {
            // stop the progress bar
            progression.done();
            // add a notification
            notification.log(`Se agrego #${entry._identifierValue} exitosamente.`, { addnCls: 'humane-flatty-success' });
            // redirect to the list view
            //$state.go($state.get('list'), { entity: entity.name() });
            $state.go($state.get('edit'), { entity: 'orders', 'id': entry.values.order });
            // cancel the default action (redirect to the edition view)
            return false;
    }]);

    return payments;
}
