export default function (nga, admin) {

    var order_status_choices = [
        {
            "label": "Cancelada",
            "value": 0
        },
        {
            "label": "Pendiente",
            "value": 1
        },
        {
            "label": "Completada",
            "value": 2
        }
    ];

    var order_status_choices_create = [
        {
            "label": "Pendiente",
            "value": 1
        },
        {
            "label": "Completada",
            "value": 2
        }
    ];

    var payment_status_choices = [
        {
            "label": "Cancelado",
            "value": 0
        },
        {
            "label": "Pendiente",
            "value": 1
        },
        {
            "label": "Pagado",
            "value": 2
        }
    ];

    var order_payment_status_choices = [

        {
            "label": "Pagado",
            "value": true
        },
        {
            "label": "Pendiente",
            "value": false
        }
    ];

    var orders = admin.getEntity('orders');
    orders.listView()
      .title('Ordenes')
        .fields([
            nga.field('created_at', 'date')
              .format('dd-MM-yyyy')
              .isDetailLink(true)
              .label('Fecha de creacion'),
            nga.field('status', 'choice').choices(order_status_choices)
              .label('Estado'),
            nga.field('payed').map(function truncate(value, entry) {
                if(value){ return 'Pagado'} else { return 'Pendiente'}
                })
              .label('Estado de Pago'),
            nga.field('total', 'amount')
              .label('Total'),
            nga.field('client', 'reference')
              .label('Cliente')
              .perPage(200)
                .targetEntity(admin.getEntity('clients'))
                .targetField(nga.field('username'))
                .singleApiCall(ids => ({'id': ids }))
        ]).filters([
        nga.field('status', 'choice').choices(order_status_choices)
          .label('Estado'),
        nga.field('client', 'reference')
          .perPage(200)
            .label('Cliente')
            .targetEntity(admin.getEntity('clients'))
            .targetField(nga.field('first_name'))
    ])

        .listActions([`<ma-create-button entity-name="payments" default-values="{ order: entry.values.id }" size="xs" label="Agregar pago"></ma-create-button>`]);
    
    orders.creationView()
        .fields([
            
            nga.field('status', 'choice').choices(order_status_choices_create)
              .defaultValue(1)
              .label('Estado'),
            nga.field('client', 'reference')
				.perPage(200)
              .validation({ validator: function(value) { if (!value) throw new Error ('Selecciona un cliente'); } })
              .label('Cliente')
              .targetEntity(admin.getEntity('clients'))
              .targetField(nga.field('username'))
              .attributes({ placeholder: 'Select client...' })
              .remoteComplete(true, {
                  refreshDelay: 300 ,
                  searchQuery: search => ({ q: search })
              }),

            nga.field('items', 'embedded_list')
              .label('Productos')
              .validation({ validator: function(value) { if (!value) throw new Error ('Es requerido agregar al menos un producto'); } })
              .targetFields([ 
                  nga.field('quantity', 'number')
                    .defaultValue(1)
                    .validation({ required: true})
                    .label('Cantidad'),
                  nga.field('product', 'reference')
                    .permanentFilters({ stock_gte: 1 })
                    .perPage(200)
                    .validation({ required: true})
                    .label('Producto')
                      .targetEntity(admin.getEntity('productsVariants'))
                      .targetField(nga.field('display_name'))
                      .attributes({ placeholder: 'Seleccione una orden...' })
                      .remoteComplete(true, {
                          refreshDelay: 300 ,
                          searchQuery: search => ({ q: search })
                      }),

              ])
            
            ])
/*
    orders.showView().fields([
        nga.field('id'),
        nga.field('total', 'amount'),
        nga.field('client', 'reference')
                .targetEntity(admin.getEntity('clients'))
                .targetField(nga.field('first_name'))
                .singleApiCall(ids => ({'id': ids })),
      nga.field('items', 'referenced_list')
          .targetEntity(admin.getEntity('orderItems'))
          .targetReferenceField('order')
          .targetFields([
              nga.field('id'),
              nga.field('quantity', 'number'),
              nga.field('product', 'reference')
                  .targetEntity(admin.getEntity('products'))
                  .targetField(nga.field('name'))
                  .attributes({ placeholder: 'Select product...' })
                  .remoteComplete(true, {
                      refreshDelay: 300 ,
                      searchQuery: search => ({ q: search })
                  }),
          ])
          .sortField('created_at')
          .sortDir('DESC')
          .listActions(['edit']),

  ])*/



    orders.editionView().actions(['show', 'list'])
        .fields([
            nga.field('created_at', 'date')
              .editable(false)
              .format('dd-MM-yyyy')
              .label('Fecha de creacion'),
            nga.field('total').editable(false),
            nga.field('status', 'choice').choices(order_status_choices)
              .label('Estado'),
//            nga.field('last_status_change', 'choice').choices(order_status_choices_create)
//              .editable(false)
//              .label("Estado anterior"),
//            nga.field('datetime_last_status_change', 'date')
//              .editable(false)
//              .format('dd-MM-yyyy')
//              .label('Fecha de cambio de estado'),


            nga.field('payed').map(function truncate(value, entry) {
                if(value){ return 'Pagado'} else { return 'Pendiente'}
                })
//                .choices(order_payment_status_choices)
              .label('Estado de Pago')
                .editable(false),



            nga.field('client', 'reference')
              .perPage(200)
              .label('Cliente')
                .editable(false)
                .targetEntity(admin.getEntity('clients'))
                .targetField(nga.field('username'))
                .singleApiCall(ids => ({'id': ids })),

            nga.field('total', 'amount')
              .editable(false)
              .label('Total'),

            nga.field('items', 'referenced_list').editable(false)
              .perPage(200)
              .label('Detalles')
                .targetEntity(admin.getEntity('orderItems'))
                .targetReferenceField('order')
                .targetFields([
                  nga.field('id'),
                  nga.field('product_sku'),
                  nga.field('product_name'),
                  nga.field('quantity'),
                  nga.field('price'),
              ]).singleApiCall(ids => ({'id': ids })),
 
            nga.field('payments', 'referenced_list').editable(false)
              .perPage(200)
              .label('Pagos')
                .targetEntity(admin.getEntity('payments'))
                .targetReferenceField('order')
                .targetFields([
                  nga.field('id'),
                  nga.field('type'),
                  nga.field('amount'),
                  nga.field('status', 'choice').choices(payment_status_choices),
              ]).singleApiCall(ids => ({'id': ids })),
            nga.field('Acciones', 'template')
              .template(`<ma-create-button entity-name="payments" default-values="{ order: entry.values.id }" size="xs" label="Agregar pago"></ma-create-button>`),

        ]);

    return orders;
}
