import TextField from 'admin-config/lib/Field/TextField';

class AttributeField extends TextField {
    constructor(name) {
        super(name);
    }
}
export default AttributeField;