function dynamicAttributeDirective($http) {
    return {
        restrict: 'E',
        scope: {
            value: '&',
            field: '&'
        },
        link: function(scope, element, attrs) {
            if(scope.value()){
                //scope.attributes_values = JSON.parse(scope.value()); //JSON.parse(scope.value());
                scope.attributes_values = eval('(' + scope.value() + ')');
            }else{
                scope.attributes_values = {};
            }
            
            scope.$watch(function(){ return scope.$parent.entry.values.product; }, function(){
                if(scope.$parent.entry.values.product){
                    $http.get(window.endpoint + 'products-attributes?products__id=' + scope.$parent.entry.values.product)
                     .then(response => {
                         scope.attributes_config = response.data.list;
                     }); 
                }
                   
            });

            element.bind('change', function(){
                scope.$apply(function(){
                    scope.$parent.entry.values.attributes = angular.copy(JSON.stringify(scope.attributes_values));
                });
            });
        },
        template: `
            <div ng-repeat="config in attributes_config" class="form-group" ng-class="{'has-error': !attributes_values[config.id], 'has-success': attributes_values[config.id] }">
                <label>{{ config.name }}</label>

                <span ng-switch="config.widget_type"  class="col-md-12">
                <div ng-switch-when="COL" class="ng-admin-field-select ng-admin-type-choice col-sm-10 col-md-8 col-lg-7">
                    <div class="radio" ng-repeat="value in config.values">
                        <label style="color: {{ value.color }}" >
                            <input type="radio" style="margin-left: -20px !important;" ng-if="config.id" ng-model="attributes_values[config.id]"  value="{{ value.id }}" name="color-{{ config.id }}" required=required ng-required="true">
                            {{ value.display }}
                      </label>
                    </div>
                    <span class="glyphicon form-control-feedback glyphicon-remove" ng-class="attributes_values[config.id] ? 'glyphicon-ok' : 'glyphicon-remove'"></span>
                </div>

                <div ng-switch-when="TXT" class="ng-admin-field-select ng-admin-type-choice col-sm-10 col-md-8 col-lg-7">
                    <select class="form-control ng-valid-pattern ng-valid-minlength ng-valid-maxlength ng-dirty ng-valid-parse ng-invalid ng-invalid-required ng-touched" ng-if="config.id" ng-model="attributes_values[config.id]" required=required ng-required="true">
                        <option ng-repeat="value in config.values" value="{{value.id}}" ng-selected="attributes_values[config.id] === '{{ value.id }}'" >
                            {{value.display}}
                        </option>
                    </select>
                    <span class="glyphicon form-control-feedback glyphicon-remove" ng-class="attributes_values[config.id] ? 'glyphicon-ok' : 'glyphicon-remove'"></span>
                </div>
                </span>
            </div>
        `
    };
}
dynamicAttributeDirective.$inject = ['$http'];

export default dynamicAttributeDirective;