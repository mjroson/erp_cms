function requestInterceptor(RestangularProvider) {
    // use the custom query parameters function to format the API request correctly
    RestangularProvider.setDefaultRequestParams('limit', '200');

    RestangularProvider.addFullRequestInterceptor(function(element, operation, what, url, headers, params) {
       //            console.log("INTERCEPTORS");
//            console.log("ELEMENT");
//            console.log(element);
//            console.log("OPERATION");
//            console.log(operation);
//            console.log("WHAT");
//            console.log(what);
//            console.log("URL");
//            console.log(url);
//            console.log("HEADERS");
//            console.log(headers);
//            console.log("PARAMS");
//            console.log(params);

        if (operation == "getList") {
            if(params._perPage){
                params['limit'] = params._perPage;
            }else{
                params['limit'] = 30;    
            }
            
            if (params._page) {
                params.page = params._page;
                delete params._page;
                delete params._perPage;
            }
            // custom sort params
            if (params._sortField) {
                if(params._sortDir != 'DESC'){
                    params.ordering = params._sortField
                }else{
                    params.ordering = "-" + params._sortField
                }
                delete params._sortField;
                delete params._sortDir;
            }
            // custom filters
            if (params._filters) {
                for (var filter_name in params._filters) {
                    params[filter_name] = params._filters[filter_name];
                }
                delete params._filters;
            }
            if (headers['Content-Range']) {
                headers['X-Total-Count'] = headers['Content-Range'].split('/').pop();
            }

        }

        return { params: params, headers: headers , url: url + '/' };
    });
}

function responseInterceptor(RestangularProvider, $stateProvider) {
    RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response){
        // console.log("INTERCEPTORS");
        // console.log("OPERATION");
        // console.log(operation);
        // console.log("WHAT");
        // console.log(what);
        // console.log("URL");
        // console.log(url);
        // console.log("RESPONSE");
        // console.log(response);
        // console.log("DATA");
        // console.log(data);
        if (operation == "getList") {
            response.totalCount = data['count'];
            return data['list'];
            
            
        }else{
            return data;
        }
    });
}

export default { requestInterceptor, responseInterceptor }
