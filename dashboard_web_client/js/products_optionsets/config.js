export default function (nga, admin) {

    var widget_types_choices = [
        {
            "label": "Text",
            "value": "TXT"
        },
        {
            "label": "Image",
            "value": "IMG"
        }
    ];

    var productsOptionsets = admin.getEntity('productsOptionsets');

    productsOptionsets.listView()
        .title('Product option sets')
        .fields([
            nga.field('id'),
            nga.field('name')
                .label('Nombre'),
            nga.field('widget_type', 'choice').choices(widget_types_choices)
                .label('Tipo'),
            nga.field('category', 'reference')
                .label('Categoria')
                .targetEntity(admin.getEntity('categories'))
                .targetField(nga.field('name'))
                .singleApiCall(ids => ({'id': ids }))
        ])
    .filters([
        nga.field('search', 'template')
            .label('')
            .pinned(true)
            .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Search" class="form-control"></input><span class="input-group-addon"><i class="fa fa-search"></i></span></div>') 
    ]);

    productsOptionsets.creationView()
        .title('Crear Set de opciones de producto')
        .fields([
            nga.field('name')
                .label('Nombre'),
            nga.field('description', 'text')
                .label('Descripcion'),
            nga.field('widget_type', 'choice').choices(widget_types_choices)
                .label('Tipo'),
            nga.field('category', 'reference')
                .perPage(200)
                .label('Categoria')
                .targetEntity(admin.getEntity('categories'))
                .targetField(nga.field('name'))
                .singleApiCall(ids => ({'id': ids }))
            ]);

    productsOptionsets.editionView()
        .title('Set de opciones  para la categoria "{{ entry.values.category_name }}"')
        .fields([
                productsOptionsets.creationView().fields(),
                
                nga.field('options', 'referenced_list') // display list of related comments
                    .perPage(200)
                    .label('Opciones')
                    .targetEntity(admin.getEntity('productsOptions'))
                    .targetReferenceField('parent')
                    .targetFields([
                        nga.field('id'),
                        nga.field('name')
                            .label('Nombre'),
                        nga.field('short_description')
                            .label('Descripcion corta')
                    ])
                    .sortField('created_at')
                    .sortDir('DESC'),
                nga.field('Acciones', 'template')
                  .template(`<ma-create-button entity-name="productsOptions" default-values="{ parent: entry.values.id }" size="xs" label="Agregar opcion"></ma-create-button>
                    <ma-filtered-list-button entity-name="productsOptions" filter="{ parent: entry.values.id }" size="xs" label="Ver listado de opciones"></ma-filtered-list-button>`)

            ]);

    
    return productsOptionsets;
}
