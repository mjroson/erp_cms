
export default function (nga, admin) {

    var productsOptions = admin.getEntity('productsOptions');

    productsOptions.listView()
        .title('Options')
        .fields([
            nga.field('id', 'number'),
            nga.field('name', 'text')
                .label('Nombre'),
            nga.field('short_description', 'text')
                .label('Descripcion corta'),
            nga.field('parent', 'reference')
                .label('Options set')
                .targetEntity(admin.getEntity('productsOptionsets'))
                .targetField(nga.field('name'))
                .singleApiCall(ids => ({'id': ids }))
        ]).filters([
        nga.field('search', 'template')
            .label('')
            .pinned(true)
            .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Buscar" class="form-control"></input><span class="input-group-addon"><i class="fa fa-search"></i></span></div>'),
        nga.field('parent', 'reference')
            .perPage(200)
            .label('Options set')
            .targetEntity(admin.getEntity('productsOptionsets'))
            .targetField(nga.field('name'))
    ]);

    productsOptions.creationView()
        .title('Crear opcion')
        .fields([
            nga.field('name')
                .label('Nombre')
                .validation({required: true }),
            nga.field('parent', 'reference')
                .perPage(200)
                .targetEntity(admin.getEntity('productsOptionsets'))
                .targetField(nga.field('name')),
            nga.field('short_description', 'text')
                .label('Descripcion corta'),
            nga.field('description', 'wysiwyg')
                .label('Descripcion')
        ]).onSubmitSuccess(['progression', 'notification', '$state', 'entry', 'entity', function(progression, notification, $state, entry, entity) {
            // stop the progress bar
            progression.done();
            // add a notification
            notification.log(`Se agrego #${entry._identifierValue} exitosamente.`, { addnCls: 'humane-flatty-success' });
            // redirect to the list view
            //$state.go($state.get('list'), { entity: entity.name() });
            $state.go($state.get('edit'), { entity: 'productsOptionsets', 'id': entry.values.parent });
            // cancel the default action (redirect to the edition view)
            return false;
    }]);

    productsOptions.editionView()
        .title('Editar {{ entry.values.name }}')
        .fields(
            productsOptions.creationView().fields(),
            
    );

    return productsOptions;
}
