from rest_framework import viewsets
from rest_framework.response import Response

from .exceptions import OrderItemException
from .models import Order, OrderItem
from .serializers import OrderItemSerializer, OrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing Order
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_fields = ('status', 'client')

    def create(self, request, *args, **kwargs):
        try:
            return super(OrderViewSet, self).create(request, *args, **kwargs)
        except OrderItemException as e:
            return Response(e.message, status=400)


class OrderItemViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing OrderItem
    """
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    filter_fields = ('order',)
    search_fields = ('product_name', 'product__sku')







