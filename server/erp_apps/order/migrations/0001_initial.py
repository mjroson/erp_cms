# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
        ('account_balance', '0001_initial'),
        ('client', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('status', models.IntegerField(choices=[(0, 'canceled'), (1, 'pending'), (2, 'completed'), (3, 'issued')], default=1)),
                ('total', models.DecimalField(decimal_places=2, editable=False, max_digits=12, default=0)),
                ('last_status_change', models.IntegerField(choices=[(0, 'canceled'), (1, 'pending'), (2, 'completed'), (3, 'issued'), (4, 'created')], default=4)),
                ('delivered', models.BooleanField(default=False)),
                ('payed', models.BooleanField(default=False)),
                ('client', models.ForeignKey(related_name='orders', to='client.Client')),
                ('ticket', models.OneToOneField(to='account_balance.Ticket', null=True)),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('quantity', models.PositiveIntegerField()),
                ('product_name', models.CharField(editable=False, max_length=20)),
                ('price', models.DecimalField(decimal_places=2, max_digits=12, editable=False)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='order.Order', related_name='items')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='product.ProductVariant')),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
    ]
