from rest_framework import serializers
from .models import OrderItem, Order, ORDER_STATUS_OPTIONS
from django.db import transaction

from erp_apps.core.serializers import BaseModelSerializer


class OrderItemSerializer(serializers.ModelSerializer):
    product_sku = serializers.CharField(source='product.sku', read_only=True)

    class Meta:
        model = OrderItem
        fields = '__all__'
        read_only_fields = ('order', 'product.sku')


class OrderSerializer(serializers.ModelSerializer):
    items = OrderItemSerializer(many=True)

    class Meta:
        model = Order
        fields = '__all__'
        read_only_fields = ('delivered', 'payed', 'total')


    def update(self, instance, validated_data):
        # Exclude items from serializer.
        validated_data.pop('items')
        return super().update(instance, validated_data)

    def get_normalize_data_items(self, items):
        """
            Se unifican todos los productos iguales
        """
        for i, item in enumerate(items):
            for i2, item2 in enumerate(items):
                if item['product'] == item2['product'] and i != i2:
                    items[i]['quantity'] += item2['quantity']
                    items.remove(item2)
                    break
        return items

    def create(self, validated_data):
        with transaction.atomic():
            items_data = validated_data.pop('items')
            order = Order.objects.create(**validated_data)
            for item_data in self.get_normalize_data_items(items_data):
                # OrderItem.objects.create(order=order, **item_data)
                order.add_item(**item_data)
            return order
