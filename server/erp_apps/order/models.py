from datetime import datetime

from django.core.urlresolvers import reverse
from django.db import models
from django.db import transaction
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from erp_apps.account_balance.models import Ticket
from erp_apps.client.models import Client
from erp_apps.core.models import BaseModel
from erp_apps.delivery.models import Delivery
from .exceptions import OrderItemException

ORDER_STATUS_OPTIONS = (
    (0, _('Canceled')),
    (1, _('Pending')),
    (2, _('Completed')),
    (3, _('Issued')),
)

ORDER_LAST_STATUS_OPTIONS = (
    (0, _('Canceled')),
    (1, _('Pending')),
    (2, _('Completed')),
    (3, _('Issued')),
    (4, _('Created'))
)


class Order(BaseModel):
    client = models.ForeignKey(Client, related_name='orders')
    status = models.IntegerField(choices=ORDER_STATUS_OPTIONS, default=1)
    datetime_last_status_change = models.DateTimeField(null=True, blank=True)
    # Transaction Saved Fields
    total = models.DecimalField(decimal_places=2, max_digits=12, editable=False, default=0)
    ticket = models.OneToOneField(Ticket, null=True)
    last_status_change = models.IntegerField(choices=ORDER_LAST_STATUS_OPTIONS, default=4)

    delivered = models.BooleanField(default=False)
    payed = models.BooleanField(default=False)

    def __init__(self, *args, **kwargs):
        super(Order, self).__init__(*args, **kwargs)
        try:
            self._status = self.objects.get(pk=self.pk).status if self.pk else 1
        except (models.ObjectDoesNotExist, AttributeError):
            self._status = 1

    def change_status(self):
        with transaction.atomic():
            self.datetime_last_status_change =  datetime.now()
            self.last_status_change = self._status
            self._status = self.status
            if self.status == 0: # Canceled order
                #self.save() # TODO: Esto va?
                for d_group in self.delivery_groups.filter(status=1):
                    d_group.status = 0
                    d_group.save()
                for payment in self.payments.filter(status=1):
                    payment.status = 0
                    payment.save()
            elif self.status == 2: # Completed Order
                for d_group in self.delivery_groups.all():
                    d_group.status = 2
                    d_group.save()

            self.save()

    def check_delivered(self):
        for item in self.items.all():
            quantity = Delivery.objects.filter(item=item, group__status=2, group__order=self)\
                .aggregate(Sum('quantity')).get('quantity__sum', 0)
            quantity_item_delivered = quantity if quantity else 0
            if item.quantity > quantity_item_delivered:
                self.delivered = False
                self.save()
                return False

        self.delivered = True
        self.save()

    def is_fully_paid(self):
        return self.payed

    def check_payed(self):
        total_payments_amount = self.payments.filter(status=2).aggregate(Sum('amount')).get('amount__sum', 0)
        self.payed = True if total_payments_amount and self.total <= total_payments_amount else False
        self.save()

    def __str__(self):
        return "#%s , $%s (%s)" %(self.id, self.total, self.get_status_display())

    def get_absolute_url(self):
        return reverse('profile:order-detail', kwargs={'pk': self.pk})

    def add_item(self, product, quantity, action='increment', *args, **kwargs):
        if product.available_stock_quantity >= quantity:
            if self.items.filter(product=product).exists():
                item = self.items.filter(product=product).first()
                item.quantity += quantity
            else:
                item = OrderItem(order=self, product=product, quantity=quantity, price=product.get_price_per_item())
            item.save()
            return item
        else:
            raise OrderItemException(message="El producto %s no tiene stock suficiente. El stock disponible es de %s"
                                             % (product, product.available_stock_quantity))

    def save(self, *args, **kwargs):
        if self._status != self.status:
            self.change_status()
        else:
            super(Order, self).save(*args, **kwargs)


class OrderItem(BaseModel):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.PROTECT)
    product = models.ForeignKey('product.ProductVariant', on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField()

    # Transaction Saved Fields
    product_name = models.CharField(max_length=20, editable=False)
    price = models.DecimalField(decimal_places=2, max_digits=12, editable=False)

    def __str__(self):
        return "(Order#%s) %s : %s" % (self.order.pk, self.product_name, self.quantity)

    def get_total(self):
        return float(self.quantity) * float(self.price)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.product_name = self.product.name
            self.price = self.product.price

        return super(OrderItem, self).save(*args, **kwargs)


@receiver(post_save, sender=OrderItem)
def order_item_post_save(sender, *args, **kwargs):
    """
        When create product variant then create stock
    """
    total_amount = 0
    order = kwargs['instance'].order
    for item in order.items.all():
        total_amount += item.quantity * item.price

    order.total = total_amount
    order.save()