

class OrderItemException(Exception):

    def __init__(self, *args, **kwargs):
        self.message = kwargs.get('message', "")
        self.error_code = kwargs.get('error_code', 2)