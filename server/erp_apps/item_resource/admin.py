from django.contrib import admin

from .models import ItemResource

admin.site.register(ItemResource)
