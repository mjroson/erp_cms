# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
        ('account_balance', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('type', models.CharField(choices=[('cash', 'cash'), ('check', 'cheque'), ('credit_card', 'credit card'), ('transder', 'transfer')], max_length=15)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=12)),
                ('status', models.IntegerField(choices=[(0, 'Canceled'), (1, 'Pending'), (2, 'Payed')], default=1)),
                ('order', models.ForeignKey(related_name='payments', to='order.Order')),
                ('ticket', models.OneToOneField(to='account_balance.Ticket', null=True)),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
    ]
