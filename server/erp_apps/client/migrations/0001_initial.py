# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account_balance', '0001_initial'),
        ('address', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('first_name', models.CharField(max_length=20)),
                ('address', models.OneToOneField(to='address.Address', null=True)),
                ('balance', models.OneToOneField(to='account_balance.Balance')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='client')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
