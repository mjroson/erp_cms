from django.db import models
from erp_apps.core.models import Person
from erp_apps.address.models import Address
from erp_apps.account_balance.models import Balance

from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save


class Client(Person):
    address = models.OneToOneField(Address, null=True)
    balance = models.OneToOneField(Balance)
    user = models.OneToOneField(User, related_name='client')

    def save(self, *args, **kwargs):
        if not self.pk:
            if not hasattr(self, 'balance'):
                balance = Balance()
                balance.save()
                self.balance = balance
            if not hasattr(self, 'address'):
                address = Address()
                address.save()
                self.address = address
        super(Client, self).save(*args, **kwargs)



@receiver(post_save, sender=User)
def user_post_save(sender, *args, **kwargs):
    if kwargs['created']:
        user = kwargs['instance']
        client, created = Client.objects.get_or_create(user=user)

