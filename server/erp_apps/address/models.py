from django.db import models
from erp_apps.core.models import BaseModel


class Address(BaseModel):
    street = models.CharField(max_length=50, blank=True)
