# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0001_initial'),
        ('order', '0001_initial'),
        ('address', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('quantity', models.PositiveIntegerField()),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DeliveryGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('status', models.IntegerField(choices=[(0, 'canceled'), (1, 'pending'), (2, 'delivered')], default=1)),
                ('address', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='address.Address', null=True)),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Distribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('date', models.DateTimeField()),
                ('employee', models.ForeignKey(related_name='Distributions', to='employee.Employee')),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='deliverygroup',
            name='distribution',
            field=models.ForeignKey(null=True, to='delivery.Distribution'),
        ),
        migrations.AddField(
            model_name='deliverygroup',
            name='order',
            field=models.ForeignKey(related_name='delivery_groups', to='order.Order'),
        ),
        migrations.AddField(
            model_name='delivery',
            name='group',
            field=models.ForeignKey(related_name='deliveries', to='delivery.DeliveryGroup'),
        ),
        migrations.AddField(
            model_name='delivery',
            name='item',
            field=models.ForeignKey(related_name='deliveries', to='order.OrderItem'),
        ),
    ]
