from django.apps import AppConfig


class DeliveryConfig(AppConfig):
    name = 'erp_apps.delivery'

    def ready(self):
        import erp_apps.delivery.signals

