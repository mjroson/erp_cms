from django.contrib import admin

from .models import Balance, Ticket

admin.site.register(Balance)
admin.site.register(Ticket)
