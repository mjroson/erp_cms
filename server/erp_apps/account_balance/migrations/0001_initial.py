# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Balance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('status', models.IntegerField(choices=[(0, 'canceled'), (1, 'pending'), (2, 'closed')], default=1)),
                ('type', models.IntegerField(choices=[(0, 'output'), (1, 'input')])),
                ('amount', models.DecimalField(decimal_places=2, max_digits=12, default=0)),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('actor_id', models.PositiveIntegerField()),
                ('actor_type', models.ForeignKey(related_name='tickets', to='contenttypes.ContentType')),
                ('balance', models.ForeignKey(to='account_balance.Balance')),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
    ]
