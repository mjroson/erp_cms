from rest_framework import serializers
from .models import ProductStock, IOProductStock, ItemResourceStock, IOItemResourceStock


class ProductStockSerializer(serializers.ModelSerializer):
    product_name = serializers.SerializerMethodField()
    product_sku = serializers.CharField(source='item.sku')
    reserved_stock = serializers.IntegerField(read_only=True)

    def get_product_name(self, obj):
        return obj.item.display_name

    class Meta:
        model = ProductStock
        fields = '__all__'


class IOProductStockSerializer(serializers.ModelSerializer):

    class Meta:
        model = IOProductStock
        fields = '__all__'


class ItemResourceStockSerializer(serializers.ModelSerializer):

    class Meta:
        model = ItemResourceStock
        fields = '__all__'


class IOItemResourceStockSerializer(serializers.ModelSerializer):

    class Meta:
        model = IOItemResourceStock
        fields = '__all__'


