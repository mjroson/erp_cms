from rest_framework import serializers
from ..models import OptionSet, Option


class OptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Option
        fields = '__all__'


class OptionSetSerializer(serializers.ModelSerializer):
    #options = OptionSerializer(many=True)
    category_name = serializers.SerializerMethodField()

    def get_category_name(self, obj):
        return obj.category.name

    class Meta:
        model = OptionSet
        fields = '__all__'
