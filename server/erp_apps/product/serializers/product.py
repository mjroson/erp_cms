from rest_framework import serializers
from ..models import Product, ProductImage, ProductVariant


class ProductImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductImage
        exclude = ('deleted', )
        #fields = '__all__'
        read_only_fields = ('default', )


class ProductVariantSerializer(serializers.ModelSerializer):
    stock_quantity = serializers.IntegerField(read_only=True)
    stock = serializers.IntegerField(source='stock.id', read_only=True)
    display_name = serializers.CharField(read_only=True)
    attributes_display = serializers.CharField(read_only=True)

    class Meta:
        model = ProductVariant
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    #variantions = ProductVariantSerializer(many=True)
    images = ProductImageSerializer(many=True, required=False, read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
