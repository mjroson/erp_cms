from rest_framework import serializers
from ..models import ProductAttribute, AttributeChoiceValue
from django.db import transaction


class AttributeChoiceValueSerializer(serializers.ModelSerializer):

    class Meta:
        model = AttributeChoiceValue
        fields = '__all__'
        read_only_fields = ('attribute', )


class ProductAttributeSerializer(serializers.ModelSerializer):
    values = AttributeChoiceValueSerializer(many=True)

    class Meta:
        model = ProductAttribute
        fields = '__all__'

    def create(self, validated_data):
        with transaction.atomic():
            values = validated_data.pop('values')
            attribute = ProductAttribute.objects.create(**validated_data)
            for value_data in values:
                val = AttributeChoiceValue(attribute=attribute, **value_data)
                val.save()
            return attribute