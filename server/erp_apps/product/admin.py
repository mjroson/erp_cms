from django.contrib import admin

from .models import Product, ProductImage, ProductVariant, Category, AttributeChoiceValue, \
    ProductAttribute, OptionSet, Option

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(ProductImage)
admin.site.register(ProductVariant)
admin.site.register(ProductAttribute)
admin.site.register(AttributeChoiceValue)
admin.site.register(OptionSet)
admin.site.register(Option)
