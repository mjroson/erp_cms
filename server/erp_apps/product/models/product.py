from django.db import models
from erp_apps.core.models import BaseModel

from django.template.defaultfilters import slugify
from django.db import transaction
from django.db.models import ObjectDoesNotExist
from django.utils.encoding import smart_text
from django.core.urlresolvers import reverse
from apps.products.utils import get_app_product_namespace


import logging
logger = logging.getLogger(__name__)


class Product(BaseModel):
    name = models.CharField(max_length=40)
    category = models.ForeignKey('product.Category', related_name='products', on_delete=models.PROTECT)
    price = models.DecimalField(decimal_places=2, max_digits=12)
    description = models.TextField()
    attributes = models.ManyToManyField('product.ProductAttribute', related_name='products', blank=True)
    outstanding = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_slug(self):
        return slugify(smart_text(self.name))

    def get_default_image(self):
        if self.images.filter(default=True).count():
            image = self.images.filter(default=True).first()
        else:
            image = self.images.first()
        return image.image if image else None

    def generate_all_variations(self):
        attributes = self.attributes.all()
        array_attributes = []
        for attr in attributes:
            arra_attr = []
            for value in attr.values.all():
                arra_attr.append({str(attr.id): str(value.id), 'name': value.display })
            array_attributes.append(arra_attr)

        import itertools
        for variation in itertools.product(*array_attributes):
            variation_obj = ProductVariant()
            variation_obj.product = self
            variation_obj.price = self.price
            variation_obj.attributes = {}
            variation_obj.name = ''
            variation_obj.sku = '%s-' % self.id
            for attr in variation:
                for key, value in attr.items():
                    if key == 'name':
                        variation_obj.name += ' %s' % value
                        variation_obj.sku += str(value[0]).upper()
                    else:
                        variation_obj.attributes[key] = value
            variation_obj.save()

    def get_absolute_url(self):
        prod_namespace = get_app_product_namespace()
        if prod_namespace:
            return reverse(prod_namespace +':details', kwargs={'slug': self.get_slug(), 'product_id': self.pk})
        else:
            return '#'

from jsonfield import JSONField

class ProductVariant(BaseModel):
    product = models.ForeignKey(Product, related_name='variations')
    sku = models.CharField(max_length=20) # TODO: This field is unique
    name = models.CharField(max_length=20)
    price = models.DecimalField(decimal_places=2, max_digits=12) # validators=[models.validators.MinValueValidator(0)])
    attributes = JSONField(default={})

    @property
    def display_name(self):
        return '%s %s' % (self.product.name, self.name)

    @property
    def reserved_stock_quantity(self):
        return self.stock.reserved_stock

    @property
    def stock_quantity(self):
        return self.stock.quantity

    @property
    def available_stock_quantity(self):
        return self.stock.available_stock

    def get_price_per_item(self):
        return self.price

    @property
    def attributes_display(self):
        value = ''
        for key, value in self.attributes.items():
            try:
                attr = self.product.attributes.get(pk=key)
                choice = attr.values.get(pk=value, attribute=attr)
                if not value:
                    value += ', %s : %s' % (attr.display, choice.display)
                else:
                    value += '%s : %s' % (attr.display, choice.display)
            except ObjectDoesNotExist as e:
                # TODO: QUe pasa cuando no encuentra un attributo
                logger.debug("""
                        ProductVariant.attributes_display: Objects doesnt exist. Attribute id: %s
                         Choice id: %s
                        """ % (key, value))
                logger.debug(e)
                logger.debug(50*"====")

        return value

    def __str__(self):
        return self.display_name



class ProductImage(BaseModel):
    image = models.ImageField(upload_to='product')
    default = models.BooleanField(default=False)
    product = models.ForeignKey(Product, related_name='images')

    def set_default(self):
        with transaction.atomic():
            for image in self.objects.filter(default=True, product=self.product).exclude(pk=self.pk):
                image.default = False
                image.save()

            self.default = True
            self.save()

    def save(self, *args, **kwargs):
        if not self.objects.filter(product=self.product).count():
            self.default = True
        return super(ProductImage, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-default', 'created_at']
