from django.db import models
from erp_apps.core.models import BaseModel

import itertools
from django.template.defaultfilters import slugify


TEXT = 'TXT'
IMAGE = 'IMG'
COLOR = 'COL'
WIDEGET_TYPE_CHOICES = (
    (TEXT, 'Text'),
    #(IMAGE, 'Image'),
    (COLOR, 'Color'),
)

class ProductAttribute(BaseModel):
    name = models.SlugField(max_length=50, unique=True, editable=False)
    display = models.CharField(max_length=100)
    widget_type = models.CharField(max_length=3, choices=WIDEGET_TYPE_CHOICES, default=TEXT)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.display

    def __init__(self, *args, **kwargs):
        super(ProductAttribute, self).__init__(*args, **kwargs)
        self.old_name = self.name

    def get_formfield_name(self):
        return slugify('attribute-%s' % self.name)

    def has_values(self):
        return self.values.exists()

    def save(self, *args, **kwargs):
        # Generate slug
        if not self.pk or self.name != self.old_name:
            self.slug = orig = slugify(self.name)
            for x in itertools.count(1):
                qs = ProductAttribute.objects.filter(slug=self.slug)
                qs = qs.exclude(pk=self.pk) if self.pk else qs
                if not qs.exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        super(ProductAttribute, self).save(*args, **kwargs)


class AttributeChoiceValue(BaseModel):
    display = models.CharField(max_length=100)
    color = models.CharField(max_length=7, blank=True, null=True) #validators=[RegexValidator('^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$')],
    image = models.ImageField(blank=True, null=True)
    attribute = models.ForeignKey(ProductAttribute, related_name='values')

    def __str__(self):
        return self.display