from django.db import models
from erp_apps.core.models import BaseModel

import itertools
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from apps.products.utils import get_app_product_namespace


class Category(BaseModel):
    name = models.CharField(max_length=40)
    slug = models.SlugField(auto_created='name')

    def __init__(self, *args, **kwargs):
        super(Category, self).__init__(*args, **kwargs)
        self.old_name = self.name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        prod_namespace = get_app_product_namespace()
        if prod_namespace:
            return reverse( prod_namespace + ':search') + '?category=%s' %(self.id)
        else:
            return '#'

    def save(self, *args, **kwargs):
        # Generate slug
        if not self.pk or self.name != self.old_name:
            self.slug = orig = slugify(self.name)
            for x in itertools.count(1):
                qs = Category.objects.filter(slug=self.slug)
                qs = qs.exclude(pk=self.pk) if self.pk else qs
                if not qs.exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        super(Category, self).save(*args, **kwargs)