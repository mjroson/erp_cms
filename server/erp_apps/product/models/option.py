from django.db import models

from erp_apps.core.models import BaseModel


TEXT = 'TXT'
IMAGE = 'IMG'
WIDEGET_TYPE_CHOICES = (
    (TEXT, 'Text'),
    (IMAGE, 'Image'),
)


class OptionSet(BaseModel):
    name = models.CharField(max_length=40, unique=True)
    description = models.TextField(blank=True)
    image = models.ImageField(upload_to='option_set', null=True, blank=True)
    category = models.ForeignKey('product.Category', related_name='option_sets')
    required = models.BooleanField(default=False)
    widget_type = models.CharField(max_length=3, choices=WIDEGET_TYPE_CHOICES, default=TEXT)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Option Set"
        verbose_name_plural = "Option Sets"
        ordering = ["-name"]


class Option(BaseModel):
    parent = models.ForeignKey('OptionSet', related_name='options')
    name = models.CharField(max_length=40, unique=True)
    description = models.TextField(blank=True)
    short_description = models.CharField(max_length=255, blank=True)
    image = models.ImageField(upload_to='option', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Option"
        verbose_name_plural = "Options"
        ordering = ["-name"]


