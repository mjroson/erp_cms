from rest_framework import viewsets

from ..models import ProductAttribute, AttributeChoiceValue
from ..serializers import ProductAttributeSerializer, AttributeChoiceValueSerializer


class ProductAttributeViewSet(viewsets.ModelViewSet):
    queryset = ProductAttribute.objects.all()
    serializer_class = ProductAttributeSerializer
    search_fields = ('name',)
    filter_fields = ('products__id', )


class AttributeChoiceValueViewSet(viewsets.ModelViewSet):
    queryset = AttributeChoiceValue.objects.all()
    serializer_class = AttributeChoiceValueSerializer
    search_fields = ('display',)