import django_filters

from rest_framework import filters
from rest_framework import viewsets
from rest_framework.decorators import detail_route

from ..models import Product, ProductVariant, ProductImage
from ..serializers import ProductSerializer, ProductVariantSerializer, ProductImageSerializer


class ProductImageViewSet(viewsets.ModelViewSet):
    serializer_class = ProductImageSerializer
    queryset = ProductImage.objects.all()

    @detail_route(methods=['get'])
    def default(self, request, *args, **kwargs):
        self.get_object().set_default()
        return self.retrieve(request, *args, **kwargs)



class ProductVariantFilter(filters.FilterSet):
    stock_lte = django_filters.MethodFilter(action='filter_by_stock_lte', distinct=True)
    price_lte = django_filters.MethodFilter(action='filter_by_price_lte', distinct=True)
    price_gte = django_filters.MethodFilter(action='filter_by_price_gte', distinct=True)
    #min_price = django_filters.NumberFilter(name="price", lookup_expr='gte')
    #max_price = django_filters.NumberFilter(name="price", lookup_expr='lte')

    def filter_by_stock_lte(self, queryset, value):
        return queryset.filter(stock__quantity__lte=value)

    def filter_by_price_lte(self, queryset, value):
        return queryset.filter(price__lte=value)

    def filter_by_price_gte(self, queryset, value):
        return queryset.filter(price__gte=value)

    class Meta:
        model = ProductVariant



class ProductViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing Products
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    search_fields = ('name', 'category__name', 'variations__name')
    filter_fields = ('category', )

    def perform_create(self, serializer):
        serializer.save()
        if self.request.data.get('generate_variation'):
            serializer.instance.generate_all_variations()


class ProductVariantViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing Products variants
    """
    queryset = ProductVariant.objects.all()
    serializer_class = ProductVariantSerializer
    filter_class = ProductVariantFilter
    search_fields = ('name', 'sku', 'product__name')
    filter_fields = ('stock_lte', 'product', 'product__category', 'price')

