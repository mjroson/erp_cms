from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from ..models import Category
from ..serializers import CategorySerializer
from django.db.models import ProtectedError

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    search_fields = ('name',)

    def destroy(self, request, *args, **kwargs):
        try:
            return super().destroy(request, *args, **kwargs)
        except ProtectedError:
            data = 'Esta categoria esta siendo utilizada!'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=data)
        except:
            raise





