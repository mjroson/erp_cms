from rest_framework import viewsets

from ..models import OptionSet, Option
from ..serializers import OptionSetSerializer, OptionSerializer



class OptionSetViewSet(viewsets.ModelViewSet):
    queryset = OptionSet.objects.all()
    serializer_class = OptionSetSerializer
    search_fields = ('name',)

class OptionViewSet(viewsets.ModelViewSet):
    queryset = Option.objects.all()
    serializer_class = OptionSerializer
    search_fields = ('name',)