# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AttributeChoiceValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('display', models.CharField(max_length=100)),
                ('color', models.CharField(blank=True, max_length=7)),
                ('image', models.ImageField(blank=True, upload_to='', null=True)),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('slug', models.SlugField(auto_created='name')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=40)),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Option',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(unique=True, max_length=40)),
                ('description', models.TextField(blank=True)),
                ('short_description', models.CharField(blank=True, max_length=255)),
                ('image', models.ImageField(blank=True, upload_to='option', null=True)),
            ],
            options={
                'verbose_name': 'Option',
                'verbose_name_plural': 'Options',
                'ordering': ['-name'],
            },
        ),
        migrations.CreateModel(
            name='OptionSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(unique=True, max_length=40)),
                ('description', models.TextField(blank=True)),
                ('image', models.ImageField(blank=True, upload_to='option_set', null=True)),
                ('required', models.BooleanField(default=False)),
                ('widget_type', models.CharField(max_length=3, choices=[('TXT', 'Text'), ('IMG', 'Image')], default='TXT')),
                ('category', models.ForeignKey(related_name='option_sets', to='product.Category')),
            ],
            options={
                'verbose_name': 'Option Set',
                'verbose_name_plural': 'Option Sets',
                'ordering': ['-name'],
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=40)),
                ('price', models.DecimalField(decimal_places=2, max_digits=12)),
                ('description', models.TextField()),
                ('outstanding', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProductAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.SlugField(unique=True)),
                ('display', models.CharField(max_length=100)),
                ('widget_type', models.CharField(max_length=3, choices=[('TXT', 'Text'), ('COL', 'Color')], default='TXT')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('image', models.ImageField(upload_to='product')),
                ('default', models.BooleanField(default=False)),
                ('product', models.ForeignKey(related_name='images', to='product.Product')),
            ],
            options={
                'ordering': ['-default', 'created_at'],
            },
        ),
        migrations.CreateModel(
            name='ProductVariant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('sku', models.CharField(max_length=20)),
                ('name', models.CharField(max_length=20)),
                ('price', models.DecimalField(decimal_places=2, max_digits=12)),
                ('attributes', jsonfield.fields.JSONField(default={})),
                ('product', models.ForeignKey(related_name='variations', to='product.Product')),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='product',
            name='attributes',
            field=models.ManyToManyField(blank=True, related_name='products', to='product.ProductAttribute'),
        ),
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(related_name='products', to='product.Category'),
        ),
        migrations.AddField(
            model_name='option',
            name='parent',
            field=models.ForeignKey(related_name='options', to='product.OptionSet'),
        ),
        migrations.AddField(
            model_name='attributechoicevalue',
            name='attribute',
            field=models.ForeignKey(related_name='values', to='product.ProductAttribute'),
        ),
    ]
