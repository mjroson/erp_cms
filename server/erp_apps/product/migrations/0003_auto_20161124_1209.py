# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_auto_20161114_1733'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productattribute',
            name='name',
            field=models.SlugField(editable=False, unique=True),
        ),
    ]
