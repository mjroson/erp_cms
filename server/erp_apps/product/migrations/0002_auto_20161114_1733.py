# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attributechoicevalue',
            name='color',
            field=models.CharField(blank=True, null=True, max_length=7),
        ),
    ]
