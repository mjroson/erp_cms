# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('custom_site', '0001_initial'),
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ThemeConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('config', jsonfield.fields.JSONField(default={})),
                ('custom_site', models.OneToOneField(to='custom_site.CustomSite', related_name='custom_theme', editable=False)),
                ('site', models.OneToOneField(to='sites.Site', related_name='custom_theme', editable=False)),
            ],
        ),
    ]
