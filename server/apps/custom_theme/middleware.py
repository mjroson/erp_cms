from erp_cms.settings import THEMES



class CheckConfigThemeMiddleware(object):

    def process_request(self, request):
        theme = THEMES[0]

        for key, value in theme['config'].items():
            if not request.session.get(key):
                request.session[key] = value['default']
