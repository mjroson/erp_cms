from django.conf import settings
from django.db.models import ObjectDoesNotExist
from .models import ThemeConfig


def custom_theme_preprocessor(request):
    try:
        theme = ThemeConfig.objects.get(site=settings.SITE_ID)
    except ObjectDoesNotExist:
        theme = ThemeConfig()

    return {'theme_config': theme.get_config_json() }