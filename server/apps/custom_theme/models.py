from django.db import models
from django.conf import settings

from django.contrib.sites.models import Site
from apps.custom_site.models import CustomSite

from jsonfield import JSONField


class ThemeConfig(models.Model):
    config = JSONField(default={})
    site = models.OneToOneField(Site, related_name='custom_theme', editable=False)
    custom_site = models.OneToOneField(CustomSite,
                                       related_name='custom_theme',
                                       editable=False)

    def get_config_json(self):
        return self.config
        # if self.config:
        #     return json.loads(self.config.replace("\'", '"'))
        # else:
        #     return {}

    # def __init__(self, *args, **kwargs):
    #     # try:
    #     #     kwargs['custom_site'] = CustomSite.objects.get(site=settings.SITE_ID)
    #     # except CustomSite.DoesNotExist:
    #     #     #TODO: Que pasa cuando no existe un custom site? ¿Siempre tiene que existir uno?
    #     #     pass
    #     super(ThemeConfig, self).__init__(*args, **kwargs)
    #     #if not self.custom_site:
    #     #    self.custom_site = CustomSite.objects.get(site=settings.SITE_ID)
    #     config = {}
    #     if not self.config:
    #         for key, value in self.get_theme_config()['config'].items():
    #             if not config.get(key):
    #                 config[key] = value['default']
    #
    #         self.config = config

    @property
    def path(self):
        # If default theme return none.
        if self.custom_site and self.custom_site.theme:
            if self.custom_site.theme != 'default':
                return self.custom_site.theme

        return None


    def __str__(self):
        return self.get_theme_choice()

    def get_theme_choice(self):
        if self.custom_site and self.custom_site.theme:
            return self.custom_site.theme
        else:
            return 'default'

    def get_theme_config(self):
        config = {
            'config': {}
        }
        theme = self.get_theme_choice()
        for theme_config in settings.CUSTOM_SITE_THEMES_CONFIG:
            if theme_config['name'] == theme:
                config  = theme_config

        return config

    def check_config(self):
        theme_config = self.get_theme_config()
        if not self.config and theme_config:
            for key, value in theme_config['config'].items():
                if not self.config.get(key):
                    self.config[key] = value['default']

    def save(self, *args, **kwargs):
        self.check_config()
        self.site = self.custom_site.site
        super(ThemeConfig, self).save(*args, **kwargs)
