import os
from django.core.exceptions import SuspiciousFileOperation

from django.utils._os import safe_join

from ..models import ThemeConfig
from django.conf import settings
from django.template.loaders.app_directories import Loader as BaseLoader
from django.db.models import ObjectDoesNotExist


class Loader(BaseLoader):
    is_usable = True

    def get_template_sources(self, template_name, template_dirs=None):
        """
        Returns the absolute paths to "template_name" by theme
        """
        try:
            custom_theme = ThemeConfig.objects.get(site=settings.SITE_ID)
            theme_path = custom_theme.path
        except ObjectDoesNotExist:
            theme_path = None

        if theme_path:
            custom_theme_dir = os.path.join(settings.BASE_DIR, '../', 'templates/%s' % theme_path)

            if not template_dirs:
                template_dirs = [custom_theme_dir, ]
            else:
                template_dirs += [custom_theme_dir, ]

            for template_dir in template_dirs:
                try:
                    yield safe_join(template_dir, template_name)
                except SuspiciousFileOperation:
                    # The joined path was located outside of this template_dir
                    # (it might be inside another one, so this isn't fatal).
                    pass
