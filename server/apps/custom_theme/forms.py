from django import forms

from .models import ThemeConfig


class ThemeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ThemeForm, self).__init__(*args, **kwargs)
        #if not self.instance:
            # TODO: Que pasa si hay un error en la query?
        #    self.instance = ThemeConfig(custom_site=CustomSite.objects.get(site=settings.SITE_ID))
        instance_config = self.instance.get_config_json()
        if self.instance.get_theme_config()['config']:
            for key, conf in self.instance.get_theme_config()['config'].items():
                if conf['type'] == 'option':
                    if conf['widget'] == 'radio':
                        self.fields[key] = forms.ChoiceField(choices=conf['options'],
                                                             initial=instance_config[key],
                                                             widget=forms.RadioSelect())
        else:
            self.fields['display'] = forms.Field(label='No hay configuraciones para este tema')

    def save(self, commit=True):
        config_theme = {}
        for key, conf in self.instance.get_theme_config()['config'].items():
            config_theme[key] = self.cleaned_data[key]

        self.instance.config = config_theme
        super(ThemeForm, self).save()

    class Meta:
        model = ThemeConfig
        exclude = ('site', 'custom_site', 'config')