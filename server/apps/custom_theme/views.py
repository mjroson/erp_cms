from django.conf import settings
from django.db.models import ObjectDoesNotExist
from django.template.response import TemplateResponse
from django.views.generic import UpdateView

from .models import ThemeConfig
from .forms import ThemeForm


class ThemeUpdateView(UpdateView):
    model = ThemeConfig
    form_class = ThemeForm
    template_name = 'theme/update.html'
    success_url = 'modal-close/'

    def get_object(self, queryset=None):
        try:
            return ThemeConfig.objects.get(site=settings.SITE_ID)
        except ObjectDoesNotExist:
            return None

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            form.save()
            return TemplateResponse(request, template='modal_close.html')
        else:
            super(ThemeUpdateView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        return '/modal-close/'
