from django.conf.urls import url, include, patterns

from .views import ThemeUpdateView


urlpatterns = patterns('',
                       #url(r'^$', 'apps.custom_theme.views.theme_config', name='search'),
                        url(r'^update/$', ThemeUpdateView.as_view(), name='update'),
                       )