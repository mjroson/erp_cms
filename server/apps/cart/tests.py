from django.test import TestCase
from django.contrib.auth.models import User

from rest_framework.test import APIRequestFactory, RequestsClient
from requests.auth import HTTPBasicAuth

from rest_framework.authtoken.models import Token

from erp_apps.product.models import Product, ProductVariant, Category
from erp_apps.order.models import Order
from erp_apps.client.models import Client

api_root = 'http://testserver/api/public/v1/'
from django.middleware.csrf import _get_new_csrf_key
from rest_framework.test import APIClient
class CartAPITests(TestCase):

    def setUp(self):
        self.rest_client = RequestsClient()
        # Client
        client_user = User.objects.create_user('client_1', password='qwerty123')
        # self.rest_client.auth = HTTPBasicAuth('client_1', 'qwerty123')
        self.client, created = Client.objects.get_or_create(user=client_user)


        # Products
        category = Category.objects.create(name='cat_1')
        product_params = {'name': 'prod_1', 'category': category, 'price': 10, 'description': 'a description...'}
        self.product = Product.objects.create(**product_params)

        variant_params = {'product': self.product, 'sku': 'no_stock', 'name': 'no_stock', 'price': 10}
        self.no_stock_variant = ProductVariant.objects.create(**variant_params)

        variant_params['sku'] = 'with_stock'
        variant_params['name'] = 'with_stock'
        self.with_stock_variant = ProductVariant.objects.create(**variant_params)
        self.with_stock_variant.stock.add_stock(5)

        variant_params['sku'] = 'with_stock_2'
        variant_params['name'] = 'with_stock_2'
        self.with_stock_variant = ProductVariant.objects.create(**variant_params)
        self.with_stock_variant.stock.add_stock(5)

    def test_create_empty_cart(self):
        """
        Create a new empty cart
        """
        response = self.rest_client.get(api_root + 'cart')

        self.assertIs(response.status_code, 200)

        items = response.json().get('items')
        self.assertEqual(items, [])

    def test_cart_with_enough_stock_item(self):
        """
        Test that a cart can be created with a product with stock
        """
        quantity = 1
        cart_params = {
            "items":[
                {
                    "id": self.with_stock_variant.id,
                    "quantity": quantity
                }
            ]
        }
        response = self.rest_client.post(api_root + 'cart', json=cart_params)

        self.assertIs(response.status_code, 200)
        items = response.json().get('items')
        item = items[0]
        self.assertEqual(item.get('id'), self.with_stock_variant.id)
        self.assertEqual(item.get('quantity'), quantity)

    def test_cart_no_stock_at_all(self):
        """
        Test that cart doesn't add items with no stock
        """
        quantity = 1
        cart_params = {
            "items":[
                {
                    "id": self.no_stock_variant.id,
                    "quantity": quantity
                },
                {
                    "id": self.with_stock_variant.id,
                    "quantity": quantity
                }
            ]
        }
        response = self.rest_client.post(api_root + 'cart', json=cart_params)

        self.assertIs(response.status_code, 200)
        items = response.json().get('items')

        # Check item With Stock is present in items
        stock_item_found_flag = False
        for item in items:
            if item.get('id') == self.with_stock_variant.id:
                stock_item_found_flag = True
                break
        self.assertTrue(stock_item_found_flag, msg='Item With Stock should have been added')

        # Check No Stock is not present in items
        for item in items:
            self.assertNotEqual(item.get('id'), self.no_stock_variant.id,
                                msg='No Stock Item should\'t have been added to the cart')

    def test_cart_not_enough_stock(self):
        # TODO: Cambiar el nombre
        """
        Test that cart doesn't add items with with more quantity than their available stock.
        Steps:
            - Add item to the cart with quantity one less than their is available in stock
            - Add the same item to the cart wit quantity far more than there is availabe in stock
        """
        cart_params = {
            "items":[
                {
                    "id": self.with_stock_variant.id,
                    "quantity": self.with_stock_variant.stock.available_stock - 1
                }
            ]
        }
        response = self.rest_client.post(api_root + 'cart', json=cart_params)

        self.assertIs(response.status_code, 200)
        items = response.json().get('items')

        # Check item With Stock is present in items
        self.assertEqual(items[0].get('id'), self.with_stock_variant.id, msg='Item With Stock should have been added')

        cart_params = {
            "items":[
                {
                    "id": self.with_stock_variant.id,
                    "quantity": 1000
                }
            ]
        }
        response = self.rest_client.post(api_root + 'cart', json=cart_params)

        self.assertIs(response.status_code, 200)
        items = response.json().get('items')
        # Check item With Stock is present in items
        self.assertEqual(items[0].get('id'), self.with_stock_variant.id, msg='Item With Stock should have been added')

        # Check quantity is no bigger than available stock
        self.assertEqual(items[0].get('quantity'), self.with_stock_variant.stock.available_stock,
                         msg='Quantity should\'t be bigger that available stock')

    def test_create_order_not_enough_stock(self):
        """
        Test that order can't be created if the item doesn't have enough stock
        """
        client = APIClient()
        client.login(username='client_1', password='qwerty123')

        variant = self.with_stock_variant
        quantity = variant.stock.available_stock + 1
        cart_params = {"items": [{"id": variant.id, "quantity": quantity}]}

        response = client.post(api_root + 'cart/create_order', cart_params, format='json')

        self.assertEqual(response.status_code, 400, msg='Create order should\'ve failed')

        # TODO: Check returned cart show maximum available stock for item

    def test_create_order(self):
        client = APIClient()
        client.login(username='client_1', password='qwerty123')

        variant = self.with_stock_variant
        quantity = 1
        cart_params = {"items": [{"id": variant.id, "quantity": quantity}]}

        response = client.post(api_root + 'cart/create_order', cart_params, format='json')

        self.assertEqual(response.status_code, 201, msg='Create order should\'ve been success')

        order_id = response.data.get('id')
        order = Order.objects.get(id=int(order_id))
        self.assertIsNotNone(order, msg='Failed at creating the order')

        # Check item is in the order with the correct quantity
        order_items = order.items.all()
        self.assertEqual(len(order_items), 1)
        self.assertEqual(order_items.first().quantity, quantity)
        # TODO: Assert data is correct, like name of the product, description, attributes and options.
        # Remember It's a transaction. Transaction data should be persisted.

    def test_create_order_no_stock_at_all(self):
        client = APIClient()
        client.login(username='client_1', password='qwerty123')

        variant = self.no_stock_variant
        quantity = 1
        cart_params = {"items": [{"id": variant.id, "quantity": quantity}]}

        response = client.post(api_root + 'cart/create_order', cart_params, format='json')

        self.assertEqual(response.status_code, 400, msg='Create order should\'ve failed')

