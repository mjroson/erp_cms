from rest_framework import serializers


class StringListField(serializers.ListField):
    child = serializers.CharField()

class NewCartItemSerializer(serializers.Serializer):
    quantity = serializers.IntegerField(required=True, min_value=1)
    id = serializers.IntegerField(required=True)
    name = serializers.CharField(required=False, read_only=True)
    price = serializers.FloatField(required=False, read_only=True)
    sku = serializers.CharField(required=False, read_only=True)
    image = serializers.ImageField(required=False, allow_null=True, read_only=True)
    slug = serializers.SlugField(required=False, read_only=True)
    data = serializers.DictField(required=False, default={})

    class Meta:
        fields = '__all__'


class NewCartSerializer(serializers.Serializer):
    created_date = serializers.DateTimeField(required=False)
    updated_date = serializers.DateTimeField(read_only=True)
    items = NewCartItemSerializer(many=True, required=False, default=[])
    #total = serializers.FloatField(read_only=True)
    messages = StringListField(read_only=True)




