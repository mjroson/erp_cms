from datetime import datetime
from erp_apps.product.models import ProductVariant
from erp_apps.order.models import Order
from django.db import transaction

from .exceptions import CartException

ADD_ITEM_RETULTS = {
    'ADDED': 0,
    'NOT_ENOUGH_STOCK': 1,
    'FAILED': 2
}

class NewCartItem(object):
    def __init__(self, id, quantity, data={}):
        self.id = id
        self.quantity = quantity
        self.data = data
        try:
            self._product = ProductVariant.objects.get(pk=self.id)
            self.price = self._product.price
            self.image = self._product.product.get_default_image()
            self.sku = self._product.sku
            self.slug = self._product.product.get_slug()
            self.name = self._product.name
            self.display_name = self._product.display_name
        except ProductVariant.DoesNotExist:
            raise CartException(message='No existe la instancia del producto con ID %s.'  % self.id,
                                error_code=ADD_ITEM_RETULTS['FAILED'])

    @property
    def sub_total(self):
        return self.quantity * self.price

    def check_available_quantity(self, quantity):
        available_quantity = self._product.available_stock_quantity
        return available_quantity >= quantity, available_quantity

    def __eq__(self, other):
        return self.id == other.id and \
               self.__class__ == other.__class__ and \
               self.data == other.data


class NewCart(object):
    def __init__(self, user, created_date=None):
        self.created_date = created_date if created_date else datetime.now()
        self.updated_date = datetime.now()
        self.user = user
        self.items = []
        self.messages = []

    @property
    def total(self):
        total = float(0)
        for item in self.items:
            total += float(item.sub_total)
        return total

    def add_item(self, item_id, quantity, data, add_force=True):
        item = NewCartItem(item_id, quantity)

        if item in self.items: raise Exception("Error: Existen items repetidos")
        if not quantity > 0: raise Exception("Error: La cantidad tiene que ser un numero mayor a 0")

        check_quantity, available_quantity = item.check_available_quantity(item.quantity)

        if not available_quantity:
            msg = 'El producto %s no tiene stock.' % item.display_name
            raise CartException(message=msg, error_code=ADD_ITEM_RETULTS['NOT_ENOUGH_STOCK'])
        elif not check_quantity:
            item.quantity = available_quantity
            msg = 'El stock de producto %s no es suficiente, se agregaron (%s) %s, el maximo stock disponible.' \
                  % (item.display_name, available_quantity, item.display_name)
            #if add_force:
            raise CartException(message= msg, error_code=ADD_ITEM_RETULTS['NOT_ENOUGH_STOCK'])
        self.items.append(item)


    def create_order(self):
        with transaction.atomic():
            order = Order.objects.create(client=self.user.client)
            for item in self.items: order.add_item(item._product, quantity=item.quantity)
            return order
