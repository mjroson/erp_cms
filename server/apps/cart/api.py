from rest_framework import viewsets, status
from rest_framework.decorators import list_route
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from erp_apps.order.serializers import OrderSerializer

from .exceptions import CartException
from .models import NewCart, ADD_ITEM_RETULTS
from .serializers import NewCartSerializer


class NewCartViewSet(viewsets.ViewSet):
    permission_classes = (AllowAny,)
    """
    POST:
    Payload:
    {
      "created_date": "2016-09-16T14:32:09.638195",
      "items": [
        {
            "product": 4,
            "quantity": 11,
            "data": {}
        },
        {
            "product": 4,
            "quantity": 15,
            "data": {}
        },
        {
            "product": 5,
            "quantity": 1,
            "data": {}
        }
        ]
    }
    La cantidad y el id del producto son campos requeridos.

    Comportamientos del carrito:
        Si la cantidad no es suficiente, se le agrega la maxima cantidad sufiente y devuelve un mensaje
            Ej: El stock de producto [REMERA] no es suficiente.
        Si el ID no pertenece a una instancia de Producto, se quita del carrito y se devuelve un mensaje:
            Ej: No existe la instancia del producto con ID [15].
    """

    def list(self, request):
        cart = NewCart(user=self.request.user)
        return Response(NewCartSerializer(cart).data)

    def get_normalize_data_items(self, items):
        """
            Se unifican todos los productos iguales
        """
        for i, item in enumerate(items):
            for i2, item2 in enumerate(items):
                if item['id'] == item2['id'] and i != i2: # and item['data'] == item2['data'] # This add when exists options
                    items[i]['quantity'] += item2['quantity']
                    items.remove(item2)
                    break
        return items

    def create_cart(self):
        serializer = NewCartSerializer(data=self.request.data)
        if serializer.is_valid():
            data = serializer.data
            items = self.get_normalize_data_items(data['items'])
            self.cart = NewCart(user=self.request.user, created_date=data.get('created_date'))
            for item in items:
                try:
                    self.cart.add_item(item.get('id'), item.get('quantity'), item.get('data', {}))
                except CartException as e:
                    if e.error_code == ADD_ITEM_RETULTS['NOT_ENOUGH_STOCK']:
                        self.cart.messages.append(e.message)
                    else:
                        print("Add items to cart error: %s" % e.message)
            return None
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        response = self.create_cart()
        if response:
            return response
        else:
            return Response(NewCartSerializer(self.cart).data)


    @list_route(methods=['post'])
    def create_order(self, request):
        response = self.create_cart()
        if response:
            return response
        else:
            if len(self.cart.messages):
                # Si existe algun mensaje, fue porque alguna cantidad fue modificada.
                # Se devuelve el carrito con un 400, para que el usuario vuelva a confirmar la nueva cantidad
                return Response(NewCartSerializer(self.cart).data, status=status.HTTP_400_BAD_REQUEST)
            order = self.cart.create_order()
            #order.send_email()
            return Response(OrderSerializer(order).data, status=status.HTTP_201_CREATED)
