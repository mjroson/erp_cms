from django.conf.urls import url, patterns
from django.views.generic import TemplateView

urlpatterns = patterns('',
                       url(r'cart/$', TemplateView.as_view(template_name="cart/checkout.html"), name='cart'),
)