from django import forms

from erp_apps.address.models import Address


class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        exclude = ['deleted',]
