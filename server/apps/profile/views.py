from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.views.decorators.http import require_POST
from django.utils.translation import ugettext as _

from .forms import AddressForm
from django.views.generic import ListView, DetailView
from erp_apps.product.models import Product
from apps.favorite.models import Favorite
from erp_apps.order.models import Order


@login_required
def details(request):
    ctx = {'address': request.user.client.address }
    return TemplateResponse(request, 'userprofile/details.html', ctx)


@login_required
def orders(request):

    ctx = {'orders': request.user.client.orders.all().order_by('-created_at')}
    return TemplateResponse(request, 'userprofile/orders.html', ctx)


@login_required
def address_edit(request, pk):
    address_form = AddressForm(request.POST or None, instance=request.user.client.address)
    if address_form.is_valid():
        address_form.save()
        message = _('Address successfully updated.')
        messages.success(request, message)
        return HttpResponseRedirect(reverse('profile:details'))
    return TemplateResponse(
        request, 'userprofile/address-edit.html',
        {'address_form': address_form})


class UserFavoritesListView(ListView):
    model = Product
    template_name = 'userprofile/favorites.html'
    context_object_name = 'products'


    def get_queryset(self):
        return Product.objects.filter(id__in=Favorite.objects.for_user(self.request.user, model=Product).values_list('target_object_id'))



class OrderDetailView(DetailView):
    model = Order
    template_name = 'userprofile/order_detail.html'
