from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.details, name='details'),
    url(r'^orders/$', views.orders, name='orders'),
    url(r'^orders/(?P<pk>[0-9]+)/$', views.OrderDetailView.as_view(), name='order-detail'),
    url(r'^address/(?P<pk>\d+)/edit/$', views.address_edit,
        name='address-edit'),
    url(r'^favorites/$', views.UserFavoritesListView.as_view(), name='favorites'),
]
