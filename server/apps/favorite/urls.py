from django.conf.urls import patterns, url


urlpatterns = patterns('apps.favorite.views',
    url(r'^add-or-remove$', 'add_or_remove', name='add-or-remove'),
    url(r'^remove$', 'remove', name='remove'),
)
