# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='OverallRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('voted_id', models.PositiveIntegerField()),
                ('quantity_vote', models.PositiveIntegerField(default=0)),
                ('rate', models.DecimalField(decimal_places=1, max_digits=6, null=True)),
                ('voted_type', models.ForeignKey(related_name='overall_voted_type', to='contenttypes.ContentType')),
            ],
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('transaction_id', models.PositiveIntegerField()),
                ('state', models.IntegerField(choices=[(0, 'Pendiente'), (1, 'Aceptado'), (2, 'Anulado')], default=0)),
                ('rate', models.PositiveIntegerField(null=True, choices=[(1, 'Very bad'), (2, 'Bad'), (3, 'Medium'), (4, 'Good'), (5, 'Very Good')])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('voted_id', models.PositiveIntegerField()),
                ('trans_type', models.ForeignKey(related_name='transation_type', to='contenttypes.ContentType')),
                ('voted_type', models.ForeignKey(related_name='voted_type', to='contenttypes.ContentType')),
                ('voter', models.ForeignKey(related_name='user_voter', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
