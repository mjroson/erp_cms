from django.contrib import admin


from .models import OverallRating, Rating

admin.site.register(OverallRating)
admin.site.register(Rating)