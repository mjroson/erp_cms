# -*- coding: utf-8 -*-
from django import template
from django.db.models import ObjectDoesNotExist

from ..models import OverallRating


register = template.Library()


@register.simple_tag(takes_context=True)
def rating_product(context, product_id, count=True):
    try:
        return OverallRating.objects.get(voted_id=product_id).get_percent_rate()
    except ObjectDoesNotExist:
        return 0

@register.simple_tag(takes_context=True)
def rating_quantity_vote_product(context, product_id):
    try:
        return OverallRating.objects.get(voted_id=product_id).quantity_vote()
    except ObjectDoesNotExist:
        return 0




