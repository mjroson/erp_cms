from .models import CustomSite
from .serializers import CustomSiteSerializer
from django.conf import settings


# TODO: Cache this function
def custom_site_preprocessor(request):
    site_data = settings.SITE_DEFAULT_DATA
    try:
        site = CustomSite.objects.get(site=settings.SITE_ID)

        if site:
            site_data = CustomSiteSerializer(site).data

            for key, value in settings.SITE_DEFAULT_DATA.items():
                if not site_data.get(key):
                    site_data[key] = value
    except CustomSite.ObjectsDoesExist:
        pass

    return {'custom_site': site_data}