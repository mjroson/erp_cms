from django import forms
from django.conf import settings

from django.utils.translation import ugettext_lazy as _

from apps.custom_site.models import CustomSite


class CustomSiteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CustomSiteForm, self).__init__(*args, **kwargs)
        # Generate choices with settings theme name.
        # TODO: Need add validation
        choices = (('default', 'Default'),) + \
                  tuple(((theme['name'], theme['display_name']) for theme in settings.CUSTOM_SITE_THEMES_CONFIG))
        self.fields['theme'].widget = forms.Select(
            choices=choices)

    class Meta:
        model = CustomSite
        exclude = ('site',)
