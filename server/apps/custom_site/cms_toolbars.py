from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.utils.urlutils import admin_reverse

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models import ObjectDoesNotExist

from apps.custom_site.models import CustomSite


@toolbar_pool.register
class CustomSiteToolbar(CMSToolbar):
    watch_models = [CustomSite, ]

    def populate(self):
        menu = self.toolbar.get_or_create_menu('custom-site', _('Site'))
        try:
            custom_site = CustomSite.objects.get(site=settings.SITE_ID)
        except ObjectDoesNotExist:
            custom_site = None

        if custom_site:
            url = admin_reverse('custom_site_customsite_change', args=(custom_site.pk,))
            name=_('Edit Site Data')
            try:
                custom_theme = custom_site.custom_theme
            except:
                custom_theme = None
            if custom_theme:
                url_theme = reverse('theme:update') #admin_reverse('custom_site_customsitetheme_change', args=(custom_theme.pk,))
                name_theme = _('Edit theme')
            else:
                url_theme = reverse('theme:update')
                #url_theme = admin_reverse('custom_site_customsitetheme_add')
                name_theme = _('Create config theme')

            menu.add_modal_item(name= name_theme, url=url_theme, )
        else:
            url=admin_reverse('custom_site_customsite_add')
            name=_('Set Site Data')

        menu.add_modal_item(name= name, url=url, )


        #
        # menu.add_sideframe_item(
        #     name=_('Custom Site Contact Data list'),
        #     url=admin_reverse('custom_site_customsitecontactdata_changelist'),
        #     # url=admin_reverse('custom_css_customcss_changelist'),
        # )
