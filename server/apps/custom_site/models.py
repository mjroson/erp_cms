from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from django.contrib.sites.models import Site
from django.conf import settings


class CustomSite(models.Model):
    site = models.OneToOneField(Site, on_delete=models.CASCADE, related_name='custom_site', editable=False)
    name = models.CharField(max_length=60, unique=True)
    display_name = models.CharField(max_length=60, blank=True)
    site_url = models.URLField(max_length=60, unique=True, blank=True) # sacar del objeto Site
    slogan = models.CharField(max_length=60, blank=True)
    contact_email = models.CharField(max_length=60, blank=True)
    skype = models.CharField(max_length=60, blank=True)
    logo = models.ImageField(upload_to='custom_site', null=True, blank=True)
    theme = models.CharField(max_length=50)

    def __str__(self):
        return self.display_name

    def check_require_fields(self):
        if not self.name:
            self.name = settings.SITE_DEFAULT_DATA['name']

        if not self.display_name:
            self.display_name = settings.SITE_DEFAULT_DATA['display_name']

        if not self.site_url:
            self.site_url = settings.SITE_DEFAULT_DATA['site_url']

        if not self.theme:
            self.theme = 'default'


    def __init__(self, *args, **kwargs):
        super(CustomSite, self).__init__(*args, **kwargs)
        self._theme = self.theme

    def save(self, *args, **kwargs):
        self.check_require_fields()
        if not self.pk: #and not self.site:
            self.site = Site.objects.get(pk=settings.SITE_ID)

        super(CustomSite, self).save(*args, **kwargs)

        if self._theme and self.theme != self._theme:
            self.custom_theme.save()
            self._theme = self.theme

        return self


class CustomSiteEmail(models.Model):
    custom_site = models.ForeignKey(CustomSite, related_name='emails')
    email = models.EmailField()

    def __str__(self):
        return self.email


class CustomSitePhone(models.Model):
    contact = models.ForeignKey(CustomSite, related_name='phones')
    phone_number = PhoneNumberField()

    def __str__(self):
        return self.phone_number.__str__()


class CustomSiteEmailAccount(models.Model):
    custom_site = models.OneToOneField(CustomSite, related_name='email_account')
    company = models.CharField(max_length=20)
    email = models.EmailField()
    email_pass = models.CharField(max_length=60)
    email_host = models.CharField(max_length=20)
    email_port = models.IntegerField(help_text='GMAIL use to be 587')
    use_tls = models.BooleanField(default=True)

    def __str__(self):
        return self.email
