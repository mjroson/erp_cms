from django.contrib import admin

from django.forms import ModelForm
from django_admin_dialog.mixins import DjangoAdminDialogMixin

from .forms import CustomSiteForm
from .models import CustomSite, CustomSiteEmailAccount, CustomSitePhone, CustomSiteEmail


class CustomSitePhoneInline(admin.TabularInline):
    model = CustomSitePhone
    form = ModelForm
    extra = 1


class CustomSiteEmailInline(admin.TabularInline):
    model = CustomSiteEmail
    form = ModelForm
    extra = 1


class CustomSiteEmailAccountInline(admin.TabularInline):
    #template = 'admin/custom_site/tabular2.html'
    model = CustomSiteEmailAccount
    form = ModelForm
    extra = 0


class CustomSiteAdmin(DjangoAdminDialogMixin, admin.ModelAdmin):
    list_display = ('name',)
    inlines = [CustomSiteEmailInline, CustomSitePhoneInline, CustomSiteEmailAccountInline]
    form = CustomSiteForm
    actions = None

    def render_change_form(self, request, context, *args, **kwargs):
        extra = {
            'help_text': "Good luck Creating your Ecommerce!"
        }
        context.update(extra)
        return super(CustomSiteAdmin, self).render_change_form(request, context, *args, **kwargs)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return None


admin.site.register(CustomSite, CustomSiteAdmin)


