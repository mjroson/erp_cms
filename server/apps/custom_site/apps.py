#-*- coding: utf-8 -*-
from django.apps import AppConfig


class CustomSitesConfig(AppConfig):
    name = 'custom_site'
    verbose_name = "Custom site"