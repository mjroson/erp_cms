# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(unique=True, max_length=60)),
                ('display_name', models.CharField(blank=True, max_length=60)),
                ('site_url', models.URLField(blank=True, unique=True, max_length=60)),
                ('slogan', models.CharField(blank=True, max_length=60)),
                ('contact_email', models.CharField(blank=True, max_length=60)),
                ('skype', models.CharField(blank=True, max_length=60)),
                ('logo', models.ImageField(blank=True, upload_to='custom_site', null=True)),
                ('theme', models.CharField(max_length=50)),
                ('site', models.OneToOneField(to='sites.Site', related_name='custom_site', editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='CustomSiteEmail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('email', models.EmailField(max_length=254)),
                ('custom_site', models.ForeignKey(related_name='emails', to='custom_site.CustomSite')),
            ],
        ),
        migrations.CreateModel(
            name='CustomSiteEmailAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('company', models.CharField(max_length=20)),
                ('email', models.EmailField(max_length=254)),
                ('email_pass', models.CharField(max_length=60)),
                ('email_host', models.CharField(max_length=20)),
                ('email_port', models.IntegerField(help_text='GMAIL use to be 587')),
                ('use_tls', models.BooleanField(default=True)),
                ('custom_site', models.OneToOneField(to='custom_site.CustomSite', related_name='email_account')),
            ],
        ),
        migrations.CreateModel(
            name='CustomSitePhone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(max_length=128)),
                ('contact', models.ForeignKey(related_name='phones', to='custom_site.CustomSite')),
            ],
        ),
    ]
