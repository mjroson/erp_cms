import django_filters

from rest_framework import filters
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from erp_apps.product.models import Product, ProductVariant, Category
from .serializers import PublicProductModelSerializer, PublicVariantModelSerializer, PublicCategoryModelSerializer


class ProductVariantFilter(filters.FilterSet):
    min_price = django_filters.NumberFilter(name="price", lookup_expr='gte')
    max_price = django_filters.NumberFilter(name="price", lookup_expr='lte')

    def filter_by_stock_lte(self, queryset, value):
        return queryset.filter(stock__quantity__lte=value)

    class Meta:
        model = Product
        fields = ('min_price', 'max_price', 'category')


class ProductModelViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = PublicProductModelSerializer
    filter_class = ProductVariantFilter
    search_fields = ('name',)
    permission_classes = (AllowAny,)
    ordering_fields = ('name', 'price')
    http_method_names = ['get']



class ProductVariantModelViewSet(ModelViewSet):
    queryset = ProductVariant.objects.all()
    serializer_class = PublicVariantModelSerializer
    permission_classes = (AllowAny,)
    http_method_names = ['get']


class CategoryModelViewSet(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = PublicCategoryModelSerializer
    permission_classes = (AllowAny,)
    http_method_names = ['get']