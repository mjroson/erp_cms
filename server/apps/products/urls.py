from django.conf.urls import url, patterns
from django.views.generic import TemplateView

from .views import product_details, OptionDetailView


urlpatterns = patterns('',
                       url(r'^(?P<slug>[a-z0-9-]+?)-(?P<product_id>[0-9]+)/$', product_details, name='details'),
                       url(r'$', TemplateView.as_view(template_name="product/search.html"), name='search'),
                       url(r"^options/(?P<pk>[0-9]+)/$", OptionDetailView.as_view(), name="option-detail"),

)