# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeaturedProductPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(to='cms.CMSPlugin', serialize=False, auto_created=True, parent_link=True, related_name='products_featuredproductplugin', primary_key=True)),
                ('title', models.CharField(max_length=40)),
                ('description', models.TextField()),
                ('layout', models.CharField(max_length=10, choices=[('h', 'Horizontal'), ('v', 'Vertical')], default='h')),
                ('query', models.CharField(choices=[('favorite', 'Favorites'), ('seller', 'Seller'), ('featured', 'Featured')], max_length=40)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='OptionPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(to='cms.CMSPlugin', serialize=False, auto_created=True, parent_link=True, related_name='products_optionplugin', primary_key=True)),
                ('layout', models.CharField(max_length=10, choices=[('Image', 'Images'), ('Text', 'Text')], default='Image')),
                ('option_set', models.ForeignKey(related_name='plugins', to='product.OptionSet')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SearchProductPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(to='cms.CMSPlugin', serialize=False, auto_created=True, parent_link=True, related_name='products_searchproductplugin', primary_key=True)),
                ('title', models.CharField(max_length=40)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
