# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cms.models.fields
import apps.products.models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('product', '0003_auto_20161124_1209'),
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductDetailPlaceHolder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('details', cms.models.fields.PlaceholderField(editable=False, to='cms.Placeholder', null=True, slotname=apps.products.models.my_placeholder_slotname)),
                ('product', models.OneToOneField(to='product.Product', related_name='placeholder_detail')),
            ],
        ),
    ]
