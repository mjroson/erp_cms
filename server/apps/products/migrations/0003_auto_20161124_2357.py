# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_productdetailplaceholder'),
    ]

    operations = [
        migrations.AlterField(
            model_name='featuredproductplugin',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='featuredproductplugin',
            name='query',
            field=models.CharField(choices=[('featured', 'Featured')], max_length=40),
        ),
    ]
