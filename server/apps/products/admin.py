from django.contrib import admin
from cms.admin.placeholderadmin import PlaceholderAdminMixin
from .models import ProductDetailPlaceHolder

class ProductDetailPlaceHolderAdmin(PlaceholderAdminMixin, admin.ModelAdmin):
    pass

admin.site.register(ProductDetailPlaceHolder, ProductDetailPlaceHolderAdmin)
