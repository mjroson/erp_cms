from cms.models import Page
from django.conf import settings


def get_app_product_namespace():
    app_product = Page.objects.public().filter(site=settings.SITE_ID).filter(application_urls='ProductsApp').first()
    if app_product:
        return app_product.application_namespace
    else:
        return None

