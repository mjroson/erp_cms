from django import template
from cms.models import Page
from django.conf import settings
from django.core.urlresolvers import reverse

from ..utils import get_app_product_namespace

register = template.Library()


@register.assignment_tag
def get_product_app_url(page):
    app_product_namespace = get_app_product_namespace()
    if app_product_namespace:
        return reverse(app_product_namespace + ':' + page)
    return '#'


# @register.filter(name='get_product_app_url_namespace')
# def get_product_app_url_namespace():
#     pages = Page.objects.all().filter(site=settings.SITE_ID)
#     import ipdb
#     ipdb.set_trace()
#
#     return ''

# @register.assignment_tag(takes_context=True)
# def get_product_app_url(context):
#     # do some fancy stuff here
#     return ''

# @register.assignment_tag(takes_context=True)
# def get_product_app_url_namespace(context):
#     # do some fancy stuff here
#     # app_page = Page.objects.fi
#
#     import ipdb
#     ipdb.set_trace()
#
#
#
#     return ''