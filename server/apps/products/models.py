from django.db import models

from cms.models import CMSPlugin
from erp_apps.product.models import OptionSet, Product
from cms.models.fields import PlaceholderField


class SearchProductPlugin(CMSPlugin):
    title = models.CharField(max_length=40)

    def __str__(self):
        return self.title


QUERY_CHOICES = (
    # ('favorite', 'Favorites'),
    # ('seller', 'Seller'),
    ('featured', 'Featured'),
)

LAYOUT_CHOICES = (
    ('h', 'Horizontal'),
    ('v', 'Vertical')
)

def my_placeholder_slotname(instance):
    return 'product_description'


class ProductDetailPlaceHolder(models.Model):
    product = models.OneToOneField(Product, related_name='placeholder_detail')
    details = PlaceholderField(my_placeholder_slotname)


class FeaturedProductPlugin(CMSPlugin):
    """ Defined Plugin to show featured product, with opcion on queryset """
    title = models.CharField(max_length=40)
    description = models.TextField(blank=True)
    # TODO: Query dinamicas? o opciones que representen query?
    layout = models.CharField(choices=LAYOUT_CHOICES, max_length=10, default='h')
    query = models.CharField(choices=QUERY_CHOICES, max_length=40)

    def __str__(self):
        return self.title


LAYOUT_OPTION_CHOICES = (
    ('Image', 'Images'),
    ('Text', 'Text')
)

class OptionPlugin(CMSPlugin):
    """ Defined Plugin to show featured product, with opcion on queryset """
    layout = models.CharField(choices=LAYOUT_OPTION_CHOICES, max_length=10, default='Image')
    option_set = models.ForeignKey(OptionSet, related_name='plugins')

    def __str__(self):
        return self.option_set.name