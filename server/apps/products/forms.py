from django import forms
from django.forms import ChoiceField
from django.forms.models import ModelChoiceIterator
from django.template.loader import render_to_string
from django.utils.translation import pgettext_lazy

from django.utils.safestring import mark_safe


class AddToCartForm(forms.Form):
    """
    Class use product and cart instance.
    """
    quantity = forms.CharField()
    extra_data = forms.CharField(required=False, initial="", max_length=40)
    # extra_data = forms.ChoiceField(choices=[('op1', 'op1')])


    def __init__(self, *args, **kwargs):
        self.cart = kwargs.pop('cart')
        self.product = kwargs.pop('product')
        super(AddToCartForm, self).__init__(*args, **kwargs)


    def save(self):
        """
        Adds CartLine into the Cart instance.
        """
        product_variant = self.get_variant(self.cleaned_data)

        extra_data = self.cleaned_data.get('extra_data', "")
        # return self.cart.add(product_variant, self.cleaned_data['quantity'])
        return self.cart.add(product_variant, self.cleaned_data['quantity'], data=extra_data)

    def add_error(self, name, value):
        errors = self.errors.setdefault(name, self.error_class())
        errors.append(value)


class VariantChoiceIterator(ModelChoiceIterator):
    def __init__(self, field):
        super(VariantChoiceIterator, self).__init__(field)
        self.product = self.queryset.instance if self.queryset else None
        self.attributes = self.product.attributes.prefetch_related(
            'values') if self.product else None

    def choice(self, obj):
        label = obj.display_variant(self.attributes)
        label += ' - ' + gross(obj.get_price())
        return (self.field.prepare_value(obj), label)


class VariantChoiceField(forms.ModelChoiceField):
    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return VariantChoiceIterator(self)
    choices = property(_get_choices, ChoiceField._set_choices)



class MySelect(forms.Select):
    def render_option(self, selected_choices, option_value, option_label):
        # look at the original for something to start with
        return option_label
        #return u'<option whatever>...</option>'


class ProductForm(AddToCartForm):
    variant = VariantChoiceField(queryset=None)
    extra_data = forms.HiddenInput()

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['variant'].queryset = self.product.variants
        self.fields['variant'].empty_label = None
        self.fields.pop('extra_data')

        for category in self.product.categories.all():
            for option_set in category.option_sets.all():

                option_choices = [('null', mark_safe('<img style="height: 80px; max-width: 80px;" src="/static/img/none.png"/>')
                                    if option_set.options.first().image else "X")] if not option_set.required else []

                widget_image = True if len(option_choices) > 0 else False

                option_choices += [(option.id, mark_safe('<img style="height: 80px; max-width: 80px;" src="'+ option.image.url  +'"/>')
                                    if option.image else option.title) for option in option_set.options.all()]

                self.fields['extra_data_' + str(option_set.id)] = forms.ChoiceField(
                    label= option_set.title,
                    choices= option_choices,
                    widget=forms.RadioSelect(attrs={'class': 'radio-image' if widget_image else 'radio-text'}),
                    required=option_set.required)

        # for category in self.product.categories.all():
        #     for option_set in category.option_sets.all():
        #         option_choices = [('', '<option value=""> ------------------- </option>')] if not option_set.required else []
        #         option_choices += [(option.id,
        #                             mark_safe('<option value="'+ str(option.id) +'" '+ (('style="background: url('+ option.image.url + ') no-repeat;" class="option-img"') if option.image else '') +'>' + option.title + ' </option>'),
        #                            #mark_safe('<img width="100px" src="'+ option.image.url  +'"/>') if option.image else option.title
        #                           )for option in option_set.options.all()]
        #         self.fields['extra_data_' + str(option_set.id)] = forms.ChoiceField(
        #             label= option_set.title,
        #             choices= option_choices,
        #             widget=MySelect(attrs={'class': 'select-image'}),
        #             required=option_set.required)


    def get_variant(self, cleaned_data):
        return cleaned_data.get('variant')

    def clean(self):
        super(ProductForm, self).clean()
        extra_data = []
        for key, value in self.cleaned_data.items():
            split_key = str(key).split('_')
            if len(split_key) > 2 and split_key[0] == 'extra' and value != "null" and value != '':
                extra_data.append((int(split_key[2]), int(value)))
        self.cleaned_data['extra_data'] = str(extra_data)


class ProductVariantInline(forms.models.BaseInlineFormSet):
    error_no_items = pgettext_lazy('Product admin error', 'You have to create at least one variant')

    def clean(self):
        count = 0
        for form in self.forms:
            if form.cleaned_data:
                count += 1
        if count < 1:
            raise forms.ValidationError(self.error_no_items)


class ImageInline(ProductVariantInline):
    error_no_items = pgettext_lazy('Product admin error', 'You have to add at least one image')


def get_form_class_for_product(product):
    from ..product.models import Product
    if isinstance(product, Product):
        return ProductForm
    raise NotImplementedError


class WeightInput(forms.TextInput):
    template = 'weight_field_widget.html'

    def __init__(self, unit, *args, **kwargs):
        self.unit = unit
        super(WeightInput, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None):
        widget = super(WeightInput, self).render(name, value, attrs=attrs)
        return render_to_string(self.template, {'widget': widget,
                                                'value': value,
                                                'unit': self.unit})


class WeightField(forms.DecimalField):
    def __init__(self, unit, decimal_places, widget=WeightInput, *args,
                 **kwargs):
        self.unit = unit
        step = 10 ** -decimal_places
        if isinstance(widget, type):
            widget = widget(unit=self.unit,
                            attrs={'type': 'number', 'step': step})
        super(WeightField, self).__init__(*args, widget=widget, **kwargs)


class ProductForm(AddToCartForm):
    variant = VariantChoiceField(queryset=None)
    extra_data = forms.HiddenInput()

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['variant'].queryset = self.product.variants
        self.fields['variant'].empty_label = None
        self.fields.pop('extra_data')

        for category in self.product.categories.all():
            for option_set in category.option_sets.all():

                option_choices = [('null', mark_safe('<img style="height: 80px; max-width: 80px;" src="/static/img/none.png"/>')
                                    if option_set.options.first().image else "X")] if not option_set.required else []

                widget_image = True if len(option_choices) > 0 else False

                option_choices += [(option.id, mark_safe('<img style="height: 80px; max-width: 80px;" src="'+ option.image.url  +'"/>')
                                    if option.image else option.title) for option in option_set.options.all()]

                self.fields['extra_data_' + str(option_set.id)] = forms.ChoiceField(
                    label= option_set.title,
                    choices= option_choices,
                    widget=forms.RadioSelect(attrs={'class': 'radio-image' if widget_image else 'radio-text'}),
                    required=option_set.required)

        # for category in self.product.categories.all():
        #     for option_set in category.option_sets.all():
        #         option_choices = [('', '<option value=""> ------------------- </option>')] if not option_set.required else []
        #         option_choices += [(option.id,
        #                             mark_safe('<option value="'+ str(option.id) +'" '+ (('style="background: url('+ option.image.url + ') no-repeat;" class="option-img"') if option.image else '') +'>' + option.title + ' </option>'),
        #                            #mark_safe('<img width="100px" src="'+ option.image.url  +'"/>') if option.image else option.title
        #                           )for option in option_set.options.all()]
        #         self.fields['extra_data_' + str(option_set.id)] = forms.ChoiceField(
        #             label= option_set.title,
        #             choices= option_choices,
        #             widget=MySelect(attrs={'class': 'select-image'}),
        #             required=option_set.required)


    def get_variant(self, cleaned_data):
        return cleaned_data.get('variant')

    def clean(self):
        super(ProductForm, self).clean()
        extra_data = []
        for key, value in self.cleaned_data.items():
            split_key = str(key).split('_')
            if len(split_key) > 2 and split_key[0] == 'extra' and value != "null" and value != '':
                extra_data.append((int(split_key[2]), int(value)))
        self.cleaned_data['extra_data'] = str(extra_data)


class StandardProductForm(AddToCartForm):
    variant = VariantChoiceField(queryset=None)
    extra_data = forms.HiddenInput()

    def __init__(self, *args, **kwargs):
        super(StandardProductForm, self).__init__(*args, **kwargs)
        self.fields['variant'].queryset = self.product.variants
        self.fields['variant'].empty_label = None
        # self.fields.pop('extra_data')
        #
        # for category in self.product.categories.all():
        #     for option_set in category.option_sets.all():
        #
        #         option_choices = [('null', mark_safe('<img style="height: 80px; max-width: 80px;" src="/static/img/none.png"/>')
        #                             if option_set.options.first().image else "X")] if not option_set.required else []
        #
        #         widget_image = True if len(option_choices) > 0 else False
        #
        #         option_choices += [(option.id, mark_safe('<img style="height: 80px; max-width: 80px;" src="'+ option.image.url  +'"/>')
        #                             if option.image else option.title) for option in option_set.options.all()]
        #
        #         self.fields['extra_data_' + str(option_set.id)] = forms.ChoiceField(
        #             label= option_set.title,
        #             choices= option_choices,
        #             widget=forms.RadioSelect(attrs={'class': 'radio-image' if widget_image else 'radio-text'}),
        #             required=option_set.required)

        # for category in self.product.categories.all():
        #     for option_set in category.option_sets.all():
        #         option_choices = [('', '<option value=""> ------------------- </option>')] if not option_set.required else []
        #         option_choices += [(option.id,
        #                             mark_safe('<option value="'+ str(option.id) +'" '+ (('style="background: url('+ option.image.url + ') no-repeat;" class="option-img"') if option.image else '') +'>' + option.title + ' </option>'),
        #                            #mark_safe('<img width="100px" src="'+ option.image.url  +'"/>') if option.image else option.title
        #                           )for option in option_set.options.all()]
        #         self.fields['extra_data_' + str(option_set.id)] = forms.ChoiceField(
        #             label= option_set.title,
        #             choices= option_choices,
        #             widget=MySelect(attrs={'class': 'select-image'}),
        #             required=option_set.required)


    def get_variant(self, cleaned_data):
        return cleaned_data.get('variant')

    # def clean(self):
    #     super(StandardProductForm, self).clean()
    #     extra_data = []
    #     for key, value in self.cleaned_data.items():
    #         split_key = str(key).split('_')
    #         if len(split_key) > 2 and split_key[0] == 'extra' and value != "null" and value != '':
    #             extra_data.append((int(split_key[2]), int(value)))
    #     self.cleaned_data['extra_data'] = str(extra_data)