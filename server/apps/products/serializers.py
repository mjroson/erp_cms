from easy_thumbnails.files import get_thumbnailer
from erp_apps.product.models import Product, ProductVariant, Category, ProductImage, AttributeChoiceValue

from rest_framework import serializers

from ..favorite.models import Favorite
from ..rating.models import OverallRating


class ProductImageSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        return get_thumbnailer(obj.image)['150'].url

    class Meta:
        model = ProductImage
        fields = ('image', 'default', 'id')
        read_only_fields = ('image', 'default', 'id')



class PublicProductModelSerializer(serializers.ModelSerializer):
    is_favorite = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    images = ProductImageSerializer(many=True, required=False, read_only=True)
    slug = serializers.SerializerMethodField()

    def get_slug(self, obj):
        return obj.get_slug()

    def get_is_favorite(self, obj):
        request = self.context.get('request', None)
        if request and request.user.is_authenticated():
            return True if Favorite.objects.get_favorite(request.user, obj) else False

    def get_rating(self, obj):
        try:
            o_rating = OverallRating.objects.get(voted_id=obj.pk)
            return { 'percent_rate': o_rating.get_percent_rate(), 'quantity_vote': o_rating.quantity_vote}
        except OverallRating.DoesNotExist:
            return { 'percent_rate': 0, 'quantity_vote': 0}


    class Meta:
        model = Product
        fields = '__all__'


class PublicVariantModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductVariant
        fields = '__all__'


class PublicCategoryModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'




class ProductSerializer(serializers.ModelSerializer):
    options = serializers.SerializerMethodField()

    class Meta:
        model = Product
        exclude = ['price']

    def get_options(self, obj):
        option_sets = obj.category.option_sets.all()
        result = []
        for option_set in option_sets:
            options = []
            options_list = option_set.options.all()
            for option in options_list:
                options.append({
                    'id': option.id,
                    'parent_id': option.parent.id,
                    'name': option.name,
                    'description': option.description,
                    # TODO: Implement serializer to return different sizes of image
                    'image': {'url': option.image.url} if option.image else {}
                })
            result.append({
                'id': option_set.id,
                'name': option_set.name,
                'description': option_set.description,
                # TODO: Implement serializer to return different sizes of image
                'image': {'url': option_set.image.url} if option_set.image else {},
                'required': option_set.required,
                'options': options
            })
        return result



class ProductVariantSerializer(serializers.ModelSerializer):
    stock = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    attributes = serializers.SerializerMethodField()
    parent = ProductSerializer(read_only=True, source='product')

    class Meta:
        model = ProductVariant
        fields = '__all__'

    def get_stock(self, obj):
        return obj.available_stock_quantity

    def get_price(self, obj):
        if not obj.price:
            return obj.product.price
        else:
            return obj.price

    def get_attributes(self, obj):
        attributes = []
        if type(obj.attributes) == str:
            import json
            attrs_dict = json.loads(obj.attributes)
        else:
            attrs_dict = obj.attributes

        for (key,value) in attrs_dict.items():
            attribute_value = AttributeChoiceValue.objects.get(id=value)
            attr = {
                'id': attribute_value.attribute.id,
                'widget_type': attribute_value.attribute.widget_type,
                'name': attribute_value.attribute.name,
                'display_name': attribute_value.attribute.display,
                'value':attribute_value.pk,
                'color':attribute_value.color,
                'display_value': attribute_value.display,
            }

            attributes.append(attr)
        return attributes