from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import SearchProductPlugin, FeaturedProductPlugin, OptionPlugin
from django.utils.translation import ugettext as _
from erp_apps.product.models import Product

from django.db.models import Count

from apps.favorite.models import Favorite

from cms.models import Page
from django.conf import settings
from django.contrib import messages


# class CMSOptionPlugin(CMSPluginBase):
#     """
#         Plugin to show list options to option set
#     """
#     model = OptionPlugin  # model where plugin data are saved
#     module = _("Options")
#     name = _("Option Plugin")  # name of the plugin in the interface
#     render_template = "product/option/plugin_list.html"
#
#     # def render(self, context, instance, placeholder):
#     #     return super(CMSOptionPlugin, self).render(context, instance, placeholder)


class CMSFeaturedProductPlugin(CMSPluginBase):
    """
        Plugin to show products in carousel
    """
    model = FeaturedProductPlugin  # model where plugin data are saved
    module = _("Products")
    name = _("Product Featured")  # name of the plugin in the interface
    render_template = "product/plugin_featured_product.html"

    def save_form(self, request, form, change):
        if self.has_product_app():
            self.message_user(request, msg, messages.ERROR)
            return None
        return super(CMSFeaturedProductPlugin, self).save_form(request, form, change)

    def has_product_app(self):
        if not Page.objects.public().filter(site=settings.SITE_ID).filter(application_urls='ProductsApp').exists():
            return False
        return True

    def render(self, context, instance, placeholder):
        if self.has_product_app():
            if instance.query == 'favorite':
                #import pdb; pdb.set_trace()
                objs_fav = Favorite.objects.for_model(Product).values_list('target_object_id')[:8]
                # if objs_fav:
                #     objs_fav = objs_fav.extra(
                #         select={'num_favs': "Count('target_object_id')"},
                #         order_by=['-num_favs']
                #     ).values_list('target_object_id')[:8]

                #annotate(num_favs=Count('target_object_id')).order_by('-num_favs')#.values_list('target_object_id')[:8]
                #objs_fav = Favorite.objects.for_model(Product).annotate(num_favs=Count('target_object_id')).order_by('-num_favs').values_list('target_object_id')[:8]
                #if objs_fav:
                #    objs_fav = objs_fav.annotate(num_fav=Count('target_object_id')).order_by('-num_fav').values_list('target_object_id')[:8]
                products = Product.objects.filter(id__in=objs_fav).prefetch_related('images')[:6]
            elif instance.query == 'featured':
                products = Product.objects.filter(outstanding=True).prefetch_related('images')[:6]
            elif instance.query == 'seller':
                # TODO: Falta hacer la query
                products = Product.objects.all()[:6]
            else:
                products = Product.objects.all()[:6]
        else:
            products = []
        context.update({'featured_product_title': instance.title, 'featured_product_description': instance.description, 'featured_product_list': products })
        return context

    def _get_render_template(self, context, instance, placeholder):
        if self.has_product_app():
            if instance.layout == 'v':
                return 'product/plugin_vertical_featured_product.html'
        else:
            return 'product/need_apphook_product.html'

        return super(CMSFeaturedProductPlugin, self)._get_render_template(context, instance, placeholder)


plugin_pool.register_plugin(CMSFeaturedProductPlugin)  # register the plugin
#plugin_pool.register_plugin(CMSOptionPlugin)






# class CMSSearchProductPlugin(CMSPluginBase):
#     """
#         Plugin to show products in carousel
#     """
#     model = SearchProductPlugin  # model where plugin data are saved
#     module = _("Products")
#     name = _("Product Search Plugin")  # name of the plugin in the interface
#     render_template = "product/plugin_search.html"


#plugin_pool.register_plugin(CMSSearchProductPlugin)  # register the plugin