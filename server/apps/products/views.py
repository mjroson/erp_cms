from .serializers import ProductVariantSerializer, PublicVariantModelSerializer
from django.http.response import JsonResponse

from django.http import HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from erp_apps.product.models import Product, ProductVariant, Option
from django.views.generic import DetailView
from .models import ProductDetailPlaceHolder

def product_variants_list(request, **kwargs):
    """
        Get JSON LIST to variants products
    """
    if kwargs.get('product_id'):
            variants = ProductVariant.objects.filter(product=kwargs['product_id'])
    else:
        variants = []

    return JsonResponse(PublicVariantModelSerializer(variants, many=True).data, safe=False)


from django.db.models import ObjectDoesNotExist


class OptionDetailView(DetailView):
    model = Option
    template_name = 'product/option/detail.html'
    queryset = Option.objects.all()


def product_details(request, slug, product_id):
    # TODO: Chequear si el producto esta habilitado y si tiene stock
    product = get_object_or_404(Product.objects.all(), id=product_id)
    if product.get_slug() != slug:
        return HttpResponsePermanentRedirect(product.get_absolute_url())


    variants = product.variations.all()
    variants_json = ProductVariantSerializer(variants, many=True).data

    try:
        product_detail_placeholder = ProductDetailPlaceHolder.objects.get(product=product_id)
    except ObjectDoesNotExist:
        product_detail_placeholder = ProductDetailPlaceHolder.objects.create(product=product)

    return TemplateResponse(
        request, 'product/detail.html',
        {'product': product,
         'variants_json': variants_json,
         'featured_products': Product.objects.filter(outstanding=True).prefetch_related('images')[:8],
         'product_detail_placeholder': product_detail_placeholder
        })



