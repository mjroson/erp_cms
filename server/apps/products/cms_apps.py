from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class ProductsApp(CMSApp):
    app_name = 'product'
    name = _("Products App")  # give your app a name, this is required
    #urls = ["apps.products.urls"]  # link your app to url configuration(s)

    #menus = [ProductSubMenu, ]

    def get_urls(self, page=None, language=None, **kwargs):
        return ["apps.products.urls"]

apphook_pool.register(ProductsApp)  # register your app
