from django.db import models
from cms.models import CMSPlugin


OPTION_BUTTON_TYPE = (
    ('facebook', 'Facebook'),
    ('rss', 'RSS'),
    ('twitter', 'Twitter'),
    ('delicious', 'Delicious'),
    ('linkedin', 'Linkedin'),
    ('flickr', 'Flickr'),
    ('skype', 'Skype'),
    ('email', 'Email'),
)


class ButtonSocialPlugin(CMSPlugin):
    """
        Defined plugin to add button social network on footer
    """
    type = models.CharField(max_length=40, choices=OPTION_BUTTON_TYPE, blank=False)
    href = models.CharField(max_length=255, blank=False)


    def __str__(self):
        return self.type

