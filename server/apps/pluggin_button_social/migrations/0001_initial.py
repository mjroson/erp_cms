# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='ButtonSocialPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(to='cms.CMSPlugin', serialize=False, auto_created=True, parent_link=True, related_name='pluggin_button_social_buttonsocialplugin', primary_key=True)),
                ('type', models.CharField(choices=[('facebook', 'Facebook'), ('rss', 'RSS'), ('twitter', 'Twitter'), ('delicious', 'Delicious'), ('linkedin', 'Linkedin'), ('flickr', 'Flickr'), ('skype', 'Skype'), ('email', 'Email')], max_length=40)),
                ('href', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
