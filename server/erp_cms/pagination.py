from rest_framework import pagination
from rest_framework.response import Response


class CustomPagination(pagination.PageNumberPagination):
    """
        Custom pagination: return only page number in next and previous pages.
    """
    page_size = 8
    page_size_query_param = 'limit'
    max_page_size = 200

    def get_paginated_response(self, data):
        resp = {}
        resp['next'] = self.page.next_page_number() if self.page.has_next() else None
        resp['previous'] = self.page.previous_page_number() if self.page.has_previous() else None
        resp['count'] = self.page.paginator.count
        resp['list'] = data
        return Response(resp)