from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token


from erp_apps.account_balance.api import BalanceViewSet, TicketViewSet
from erp_apps.address.api import AddressViewSet
from erp_apps.client.api import ClientViewSet
from erp_apps.delivery.api import DeliveryGroupViewSet, DeliveryViewSet, DistributionViewSet
from erp_apps.employee.api import EmployeeViewSet
from erp_apps.item_resource.api import ItemResourceViewSet
from erp_apps.order.api import OrderItemViewSet, OrderViewSet
from erp_apps.payment.api import PaymentViewSet
from erp_apps.product.apis import (ProductViewSet, ProductVariantViewSet, ProductImageViewSet,
                                   CategoryViewSet, OptionSetViewSet, OptionViewSet, ProductAttributeViewSet,
                                   AttributeChoiceValueViewSet)
from erp_apps.provider.api import ProviderViewSet
from erp_apps.stock.api import IOItemResourceStockViewSet, IOProductStockViewSet, ItemResourceStockViewSet, ProductStockViewSet

router = DefaultRouter(trailing_slash=False)

router.register(r'balances', BalanceViewSet, base_name='account-balances')
router.register(r'tickets', TicketViewSet, base_name='tickets')
router.register(r'addresses', AddressViewSet, base_name='addresses')
router.register(r'clients', ClientViewSet, base_name='clients')
router.register(r'deliveries', DeliveryViewSet, base_name='deliveries')
router.register(r'delivery-groups', DeliveryGroupViewSet, base_name='delivery-groups')
router.register(r'distributions', DistributionViewSet, base_name='distributions')
router.register(r'employees', EmployeeViewSet, base_name='employees')
router.register(r'item-resources', ItemResourceViewSet, base_name='item-resource')
router.register(r'orders', OrderViewSet, base_name='orders')
router.register(r'order-items', OrderItemViewSet, base_name='order-item')
router.register(r'payments', PaymentViewSet, base_name='payments')

router.register(r'products', ProductViewSet, base_name='products')
router.register(r'products-images', ProductImageViewSet, base_name='products-images')
router.register(r'products-variants', ProductVariantViewSet, base_name='products-variants')
router.register(r'products-attributes', ProductAttributeViewSet, base_name='products-attributes')
router.register(r'products-attributes-values', AttributeChoiceValueViewSet, base_name='products-attributes-values')
router.register(r'products-optionsets', OptionSetViewSet, base_name='products-optionsets')
router.register(r'products-options', OptionViewSet, base_name='products-options')
router.register(r'categories', CategoryViewSet, base_name='categories')

router.register(r'providers', ProviderViewSet, base_name='providers')
router.register(r'io-resources-stock', IOItemResourceStockViewSet, base_name='io-resource-stock')
router.register(r'io-products-stock', IOProductStockViewSet, base_name='io-product-stock')
router.register(r'resources-stock', ItemResourceStockViewSet, base_name='resource-stock')
router.register(r'products-stock', ProductStockViewSet, base_name='product-stock')


urlpatterns = [
    url(r'^accounts/', include('rest_auth.urls')),
    url(r'^accounts/registration/', include('rest_auth.registration.urls')),
    url(r'^api-token-auth', obtain_jwt_token),
    url(r'^api-token-refresh', refresh_jwt_token), # Request: {"token": EXISTING_TOKEN} Response: {"token": NEW_TOKEN}
    url(r'^api-token-verify/', verify_jwt_token),
    # Include router api
    url(r'^', include(router.urls)),
    ]
