from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from apps.products.api import ProductModelViewSet, ProductVariantModelViewSet, CategoryModelViewSet
from apps.cart.api import NewCartViewSet


router = DefaultRouter(trailing_slash=False)

router.register(r'products', ProductModelViewSet, base_name='products')
router.register(r'product-variants', ProductVariantModelViewSet, base_name='products-variants')
router.register(r'categories', CategoryModelViewSet, base_name='categories')
router.register(r'cart', NewCartViewSet, base_name='cart')

urlpatterns = [
    # Include router api
    url(r'^', include(router.urls)),
    ]
