# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

#from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()


urlpatterns = [
    # url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
    #     {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^profile/', include('apps.profile.urls', namespace='profile')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^select2/', include('django_select2.urls')),
    #url(r'^products/', include('apps.products.urls', namespace='product')),
    url(r'^checkout/', include('apps.cart.urls', namespace='checkout')),
    url(r'^theme/', include('apps.custom_theme.urls', namespace='theme')),

    url(r'^favit/', include('apps.favorite.urls', namespace='favit')),
    url(r'^calification/', include('apps.rating.urls', namespace="rating")),
    url(r'^theme-config/', include('apps.custom_theme.urls', namespace="custom_theme")),
    url(r'^api/v1/', include('erp_cms.urls_api', namespace='api')),
    url(r'^api/public/v1/', include('erp_cms.urls_public_api', namespace='api-public')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('djangocms_forms.urls')),
    url(r'^', include('cms.urls')),
]

# urlpatterns += i18n_patterns('',
#     url(r'^admin/', include(admin.site.urls)),  # NOQA
#     url(r'^', include('cms.urls')),
# )

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns
